-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost
-- Généré le :  jeu. 22 août 2019 à 00:57
-- Version du serveur :  5.7.23
-- Version de PHP :  7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `bdd`
--

-- --------------------------------------------------------

--
-- Structure de la table `etablissement`
--

CREATE TABLE `etablissement` (
  `gid` int(11) NOT NULL COMMENT 'TRIAL',
  `id` varchar(24) DEFAULT NULL COMMENT 'TRIAL',
  `origine` varchar(17) DEFAULT NULL COMMENT 'TRIAL',
  `nature` varchar(23) DEFAULT NULL COMMENT 'TRIAL',
  `toponyme` varchar(70) DEFAULT NULL COMMENT 'TRIAL',
  `importance` varchar(2) DEFAULT NULL COMMENT 'TRIAL',
  `geom` text COMMENT 'TRIAL',
  `trial572` char(1) DEFAULT NULL COMMENT 'TRIAL'
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='TRIAL';

--
-- Déchargement des données de la table `etablissement`
--

INSERT INTO `etablissement` (`gid`, `id`, `origine`, `nature`, `toponyme`, `importance`, `geom`, `trial572`) VALUES
(1, 'PAISCIEN0000000220102861', 'Orthophotographie', 'Enseignement sup?rieur', 'NR', 'NR', 'POINT(3.8693812709522 43.6325624277284)', 'T'),
(2, 'PAISCIEN0000000220713602', 'Orthophotographie', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.8864630727453 43.6050020947028)', 'T'),
(3, 'PAISCIEN0000000044348552', 'Orthophotographie', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.81572103757348 43.6425930569749)', 'T'),
(4, 'PAISCIEN0000000044348628', 'BDTopo', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.89813205869295 43.6103430229496)', 'T'),
(5, 'PAISCIEN0000000044348569', 'BDTopo', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.89811687415735 43.6187079773793)', 'T'),
(6, 'PAISCIEN0000000044348624', 'BDTopo', 'Enseignement sup?rieur', 'NR', 'NR', 'POINT(3.89940971474895 43.6032886146667)', 'T'),
(7, 'PAISCIEN0000000044348625', 'BDTopo', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.90758068523227 43.6118851142721)', 'T'),
(8, 'PAISCIEN0000000044348573', 'BDTopo', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.88655677873377 43.6230878972582)', 'T'),
(9, 'PAISCIEN0000000044348570', 'BDTopo', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.89143896862353 43.6187413596636)', 'T'),
(10, 'PAISCIEN0000000044348681', 'BDTopo', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.89224238522351 43.5969751717849)', 'T'),
(11, 'PAISCIEN0000000044348626', 'Orthophotographie', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.88840665287226 43.608886962797)', 'T'),
(12, 'PAISCIEN0000000044348623', 'BDTopo', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.89222122953615 43.6142483810544)', 'T'),
(13, 'PAISCIEN0000000044348653', 'BDTopo', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.87610161831208 43.6126572566081)', 'T'),
(14, 'PAISCIEN0000000044348654', 'BDTopo', 'Enseignement sup?rieur', 'NR', 'NR', 'POINT(3.87672303482402 43.6140163836023)', 'T'),
(15, 'PAISCIEN0000000044348684', 'BDTopo', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.88649543878455 43.5915486299884)', 'T'),
(16, 'PAISCIEN0000000044348678', 'Orthophotographie', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.89636094585662 43.5928920860531)', 'T'),
(17, 'PAISCIEN0000000044348584', 'BDTopo', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.87955088800955 43.6247870136064)', 'T'),
(18, 'PAISCIEN0000000044348591', 'BDTopo', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.87990298996614 43.6249686300325)', 'T'),
(19, 'PAISCIEN0000000044348679', 'Orthophotographie', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.89702577676351 43.5929720811624)', 'T'),
(20, 'PAISCIEN0000000044348631', 'BDTopo', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.87959151623034 43.6144314211043)', 'T'),
(21, 'PAISCIEN0000000044348695', 'BDTopo', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.87073068031963 43.5956983033044)', 'T'),
(22, 'PAISCIEN0000000044348687', 'BDTopo', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.88264658594538 43.5979119804604)', 'T'),
(23, 'PAISCIEN0000000044348658', 'Orthophotographie', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.8727892234002 43.6041391814801)', 'T'),
(24, 'PAISCIEN0000000044348660', 'BDTopo', 'Enseignement secondaire', 'NR', 'NR', 'POINT(3.8711052435471 43.6089550483496)', 'T'),
(25, 'PAISCIEN0000000044348578', 'BDTopo', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.87089237044852 43.6224244838156)', 'T'),
(26, 'PAISCIEN0000000044348572', 'BDTopo', 'Enseignement sup?rieur', 'NR', 'NR', 'POINT(3.87328005770142 43.6223954278974)', 'T'),
(27, 'PAISCIEN0000000044348601', 'BDTopo', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.87360329458622 43.621741351624)', 'T'),
(28, 'PAISCIEN0000000044348641', 'BDTopo', 'Enseignement sup?rieur', 'NR', 'NR', 'POINT(3.87404306008275 43.6146723494848)', 'T'),
(29, 'PAISCIEN0000000044348649', 'BDTopo', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.87351240124897 43.60934694489)', 'T'),
(30, 'PAISCIEN0000000044348696', 'BDTopo', 'Enseignement secondaire', 'NR', 'NR', 'POINT(3.87345991117503 43.6006118953086)', 'T'),
(31, 'PAISCIEN0000000044348639', 'BDTopo', 'Enseignement sup?rieur', 'NR', 'NR', 'POINT(3.87456591454846 43.6137926106607)', 'T'),
(32, 'PAISCIEN0000000044348583', 'BDTopo', 'Enseignement secondaire', 'NR', 'NR', 'POINT(3.86713964926096 43.6159784119364)', 'T'),
(33, 'PAISCIEN0000000044348545', 'BDTopo', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.85666422556719 43.6416023925871)', 'T'),
(34, 'PAISCIEN0000000044348593', 'Orthophotographie', 'Enseignement sup?rieur', 'NR', 'NR', 'POINT(3.86693089325547 43.6192932475335)', 'T'),
(35, 'PAISCIEN0000000044348603', 'BDTopo', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.86431313282425 43.6264857564918)', 'T'),
(36, 'PAISCIEN0000000044348690', 'BDTopo', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.86479958246376 43.588422100251)', 'T'),
(37, 'PAISCIEN0000000044348683', 'BDTopo', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.86814658099787 43.5927369976284)', 'T'),
(38, 'PAISCIEN0000000044348635', 'BDTopo', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.866083277985 43.6112258542661)', 'T'),
(39, 'PAISCIEN0000000044348651', 'BDTopo', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.85339502741639 43.6108448310658)', 'T'),
(40, 'PAISCIEN0000000044348595', 'BDTopo', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.86908205422613 43.6251618686766)', 'T'),
(41, 'PAISCIEN0000000044348694', 'BDTopo', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.85497750354594 43.5993632900535)', 'T'),
(42, 'PAISCIEN0000000044348594', 'Orthophotographie', 'Enseignement secondaire', 'NR', 'NR', 'POINT(3.86250975952544 43.6166163395849)', 'T'),
(43, 'PAISCIEN0000000044348616', 'BDTopo', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.84195360989078 43.6195489979364)', 'T'),
(44, 'PAISCIEN0000000044348589', 'BDTopo', 'Enseignement sup?rieur', 'NR', 'NR', 'POINT(3.86331902449856 43.6206825406126)', 'T'),
(45, 'PAISCIEN0000000044348587', 'BDTopo', 'Enseignement sup?rieur', 'NR', 'NR', 'POINT(3.86232469175428 43.6204537788384)', 'T'),
(46, 'PAISCIEN0000000044348551', 'BDTopo', 'Enseignement sup?rieur', 'NR', 'NR', 'POINT(3.84176202410408 43.6355287971719)', 'T'),
(47, 'PAISCIEN0000000044348637', 'BDTopo', 'Enseignement sup?rieur', 'NR', 'NR', 'POINT(3.86232963840763 43.6097422013519)', 'T'),
(48, 'PAISCIEN0000000044348666', 'BDTopo', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.84080768287075 43.6070269333241)', 'T'),
(49, 'PAISCIEN0000000044348598', 'Orthophotographie', 'Enseignement sup?rieur', 'NR', 'NR', 'POINT(3.85838002537678 43.6235372295166)', 'T'),
(50, 'PAISCIEN0000000044348610', 'BDTopo', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.84563019927207 43.6166392096698)', 'T'),
(51, 'PAISCIEN0000000044348599', 'Orthophotographie', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.86183084641986 43.6170977343864)', 'T'),
(52, 'PAISCIEN0000000044348667', 'Orthophotographie', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.84459505753964 43.604038957609)', 'T'),
(53, 'PAISCIEN0000000044348619', 'BDTopo', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.84750026062372 43.6278653112693)', 'T'),
(54, 'PAISCIEN0000000044348656', 'Orthophotographie', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.85765175802842 43.6053170067087)', 'T'),
(55, 'PAISCIEN0000000044348554', 'Orthophotographie', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.81981016170647 43.6404368849047)', 'T'),
(56, 'PAISCIEN0000000044348575', 'BDTopo', 'Enseignement secondaire', 'NR', 'NR', 'POINT(3.85179882362966 43.6261157308073)', 'T'),
(57, 'PAISCIEN0000000044348613', 'BDTopo', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.83903122327062 43.6173940184563)', 'T'),
(58, 'PAISCIEN0000000044348611', 'BDTopo', 'Enseignement sup?rieur', 'NR', 'NR', 'POINT(3.8300690946663 43.6172198845755)', 'T'),
(59, 'PAISCIEN0000000044348553', 'Orthophotographie', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.81910077830128 43.6379622826359)', 'T'),
(60, 'PAISCIEN0000000044348697', 'BDTopo', 'Enseignement secondaire', 'NR', 'NR', 'POINT(3.83094670263639 43.5992285013707)', 'T'),
(61, 'PAISCIEN0000000044348670', 'BDTopo', 'Enseignement sup?rieur', 'NR', 'NR', 'POINT(3.83156255785414 43.6014841221661)', 'T'),
(62, 'PAISCIEN0000000044348559', 'BDTopo', 'Enseignement sup?rieur', 'NR', 'NR', 'POINT(3.83253233736037 43.638370466147)', 'T'),
(63, 'PAISCIEN0000000044348669', 'Orthophotographie', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.83887773213092 43.615790815614)', 'T'),
(64, 'PAISCIEN0000000044348665', 'BDTopo', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.83704192162727 43.6082977570037)', 'T'),
(65, 'PAISCIEN0000000044348557', 'BDTopo', 'Enseignement sup?rieur', 'NR', 'NR', 'POINT(3.83043600968049 43.6404273308464)', 'T'),
(66, 'PAISCIEN0000000044348664', 'BDNyme', 'Enseignement primaire', 'le petit s?minaire', '8', 'POINT(3.82330200020111 43.6131825665979)', 'T'),
(67, 'PAISCIEN0000000044348607', 'BDTopo', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.81807014917313 43.6275544840539)', 'T'),
(68, 'PAISCIEN0000000044348585', 'Orthophotographie', 'Enseignement secondaire', 'NR', 'NR', 'POINT(3.86971315459455 43.6210346875171)', 'T'),
(69, 'PAISCIEN0000000044348650', 'Orthophotographie', 'Enseignement secondaire', 'NR', 'NR', 'POINT(3.85417254851476 43.6075876648525)', 'T'),
(70, 'PAISCIEN0000000044348574', 'Orthophotographie', 'Enseignement sup?rieur', 'NR', 'NR', 'POINT(3.88488260041436 43.6174199955616)', 'T'),
(71, 'PAISCIEN0000000044348663', 'Orthophotographie', 'Enseignement secondaire', 'la colline', 'NR', 'POINT(3.84200131255205 43.6055005961687)', 'T'),
(72, 'PAISCIEN0000000201322581', 'Orthophotographie', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.81935348980115 43.6273801494339)', 'T'),
(73, 'PAISCIEN0000000208927467', 'Orthophotographie', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.83097328288707 43.633896711733)', 'T'),
(74, 'PAISCIEN0000000044348661', 'BDTopo', 'Enseignement secondaire', 'NR', 'NR', 'POINT(3.87330446165066 43.602682730581)', 'T'),
(75, 'PAISCIEN0000000044348576', 'Orthophotographie', 'Enseignement secondaire', 'NR', 'NR', 'POINT(3.87595481571671 43.6153714209564)', 'T'),
(76, 'PAISCIEN0000000220713586', 'Orthophotographie', 'Enseignement secondaire', 'NR', 'NR', 'POINT(3.86475735295577 43.6456794091643)', 'T'),
(77, 'PAISCIEN0000000223408818', 'Orthophotographie', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.86393630231361 43.6268801800481)', 'T'),
(78, 'PAISCIEN0000000223408832', 'Orthophotographie', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.87065531058571 43.6223930892209)', 'T'),
(79, 'PAISCIEN0000000223408883', 'Orthophotographie', 'Enseignement sup?rieur', 'NR', 'NR', 'POINT(3.83758118996909 43.642353968275)', 'T'),
(80, 'PAISCIEN0000000223408890', 'Orthophotographie', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.89138546842945 43.6189649569982)', 'T'),
(81, 'PAISCIEN0000000223408933', 'Fichier', 'Enseignement secondaire', 'NR', 'NR', 'POINT(3.88719932899753 43.6118554579441)', 'T'),
(82, 'PAISCIEN0000000223408943', 'Orthophotographie', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.89081022213435 43.6186907252855)', 'T'),
(83, 'PAISCIEN0000000223408946', 'BDTopo', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.81936519323719 43.6379980832878)', 'T'),
(84, 'PAISCIEN0000000223408978', 'Orthophotographie', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.89840552182976 43.6100915100031)', 'T'),
(85, 'PAISCIEN0000000223408981', 'Fichier', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.87424249975915 43.63621188686)', 'T'),
(86, 'PAISCIEN0000000223408982', 'Fichier', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.86208593920626 43.6172594799302)', 'T'),
(87, 'PAISCIEN0000000223408986', 'Orthophotographie', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.90755230452333 43.6121265059138)', 'T'),
(88, 'PAISCIEN0000000223408999', 'Fichier', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.81238851199627 43.6395747601701)', 'T'),
(89, 'PAISCIEN0000000223409002', 'Orthophotographie', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.81979059116257 43.6400213077618)', 'T'),
(90, 'PAISCIEN0000000223409006', 'Orthophotographie', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.81268071918811 43.6397354466549)', 'T'),
(91, 'PAISCIEN0000000223409014', 'Fichier', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.81996442373285 43.6403070449042)', 'T'),
(92, 'PAISCIEN0000000223409016', 'Orthophotographie', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.87458054195324 43.6361992696991)', 'T'),
(93, 'PAISCIEN0000000223409026', 'Fichier', 'Enseignement secondaire', 'NR', 'NR', 'POINT(3.83800931551852 43.6471459271774)', 'T'),
(94, 'PAISCIEN0000000223409028', 'Fichier', 'Science', 'NR', 'NR', 'POINT(3.86564366577028 43.6353583587178)', 'T'),
(95, 'PAISCIEN0000000044348546', 'Orthophotographie', 'Enseignement sup?rieur', 'NR', 'NR', 'POINT(3.85106628343782 43.6361870260644)', 'T'),
(96, 'PAISCIEN0000000223409057', 'Orthophotographie', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.81970667450051 43.6380512989888)', 'T'),
(97, 'PAISCIEN0000000223409060', 'Orthophotographie', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.89833396851809 43.6189671349502)', 'T'),
(98, 'PAISCIEN0000000044348633', 'Orthophotographie', 'Enseignement secondaire', 'NR', 'NR', 'POINT(3.88504791799091 43.6118388074519)', 'T'),
(99, 'PAISCIEN0000000223409093', 'Orthophotographie', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.87006299469055 43.6224590208886)', 'T'),
(100, 'PAISCIEN0000000223409098', 'Orthophotographie', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.89793906503103 43.6105929705073)', 'T'),
(101, 'PAISCIEN0000000223409100', 'Orthophotographie', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.89777684428116 43.6188241657435)', 'T'),
(102, 'PAISCIEN0000000223409103', 'Orthophotographie', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.88897435008043 43.6088409280476)', 'T'),
(103, 'PAISCIEN0000000223409143', 'Orthophotographie', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.90756615336406 43.6116710759687)', 'T'),
(104, 'PAISCIEN0000000223409151', 'Orthophotographie', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.8423195863019 43.6195893539181)', 'T'),
(105, 'PAISCIEN0000000223410901', 'Fichier', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.8397940089216 43.6223749681258)', 'T'),
(106, 'PAISCIEN0000000223410904', 'Orthophotographie', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.84602098488085 43.6165992754524)', 'T'),
(107, 'PAISCIEN0000000223410927', 'Orthophotographie', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.84179149992327 43.6198121033146)', 'T'),
(108, 'PAISCIEN0000000223410935', 'Orthophotographie', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.8196512860576 43.6279348986745)', 'T'),
(109, 'PAISCIEN0000000223410936', 'Orthophotographie', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.8147415581163 43.6249348895564)', 'T'),
(110, 'PAISCIEN0000000223410938', 'Orthophotographie', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.81904908668479 43.6280105243612)', 'T'),
(111, 'PAISCIEN0000000223410940', 'Fichier', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.81937966079514 43.6279981349054)', 'T'),
(112, 'PAISCIEN0000000223410942', 'Orthophotographie', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.81794512923633 43.6279954413522)', 'T'),
(113, 'PAISCIEN0000000223410943', 'Orthophotographie', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.81795646594948 43.6274005689715)', 'T'),
(114, 'PAISCIEN0000000223410945', 'Orthophotographie', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.81908928313665 43.6272687621708)', 'T'),
(115, 'PAISCIEN0000000223410948', 'Orthophotographie', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.81537978138137 43.6250425822074)', 'T'),
(116, 'PAISCIEN0000000223410950', 'Orthophotographie', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.81962473065745 43.6274635883261)', 'T'),
(117, 'PAISCIEN0000000223410951', 'Orthophotographie', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.81374126554208 43.6251502428045)', 'T'),
(118, 'PAISCIEN0000000223410957', 'Orthophotographie', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.81433917640133 43.6249487028335)', 'T'),
(119, 'PAISCIEN0000000223410960', 'Orthophotographie', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.83904581557018 43.6161053564586)', 'T'),
(120, 'PAISCIEN0000000223410983', 'Orthophotographie', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.8455175267095 43.6163053511897)', 'T'),
(121, 'PAISCIEN0000000223411013', 'Fichier', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.888716983734 43.6088547309505)', 'T'),
(122, 'PAISCIEN0000000223411021', 'Orthophotographie', 'Enseignement secondaire', 'NR', 'NR', 'POINT(3.84833907769605 43.6178265652504)', 'T'),
(123, 'PAISCIEN0000000223411027', 'Orthophotographie', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.86449568983492 43.6265985836675)', 'T'),
(124, 'PAISCIEN0000000223411051', 'Orthophotographie', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.86244754322028 43.6173384953645)', 'T'),
(125, 'PAISCIEN0000000223411056', 'Orthophotographie', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.86439829804442 43.6262187284112)', 'T'),
(126, 'PAISCIEN0000000223411562', 'BDTopo', 'Science', 'NR', 'NR', 'POINT(3.86371515639596 43.6376133994593)', 'T'),
(127, 'PAISCIEN0000000224178588', 'Fichier', 'Enseignement secondaire', 'NR', 'NR', 'POINT(3.91811587172593 43.5983306948682)', 'T'),
(128, 'PAISCIEN0000000224270943', 'Orthophotographie', 'Enseignement secondaire', 'NR', 'NR', 'POINT(3.84708460411144 43.5967022755922)', 'T'),
(129, 'PAISCIEN0000000224270967', 'Orthophotographie', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.8643438197993 43.5884905140026)', 'T'),
(130, 'PAISCIEN0000000224270968', 'Orthophotographie', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.86518722949127 43.588269644925)', 'T'),
(131, 'PAISCIEN0000000224270976', 'Orthophotographie', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.86857497517395 43.5925761059673)', 'T'),
(132, 'PAISCIEN0000000224270974', 'Orthophotographie', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.87143442553733 43.5953246360616)', 'T'),
(133, 'PAISCIEN0000000224270973', 'Orthophotographie', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.87072496540572 43.5951584596935)', 'T'),
(134, 'PAISCIEN0000000224270975', 'Orthophotographie', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.86849185357135 43.5929735883822)', 'T'),
(135, 'PAISCIEN0000000224270977', 'Orthophotographie', 'Enseignement secondaire', 'NR', 'NR', 'POINT(3.86707380343496 43.5899075559976)', 'T'),
(136, 'PAISCIEN0000000224270988', 'Orthophotographie', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.88628977852761 43.5914585269026)', 'T'),
(137, 'PAISCIEN0000000224270987', 'Orthophotographie', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.88669182114116 43.591839467263)', 'T'),
(138, 'PAISCIEN0000000224270986', 'Orthophotographie', 'Enseignement secondaire', 'NR', 'NR', 'POINT(3.8912527216771 43.5970282858733)', 'T'),
(139, 'PAISCIEN0000000224271047', 'Orthophotographie', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.89631650531195 43.5931381028878)', 'T'),
(140, 'PAISCIEN0000000224271060', 'Orthophotographie', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.84432633400579 43.6040374467948)', 'T'),
(141, 'PAISCIEN0000000224271059', 'Orthophotographie', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.84394307309427 43.6042464825307)', 'T'),
(142, 'PAISCIEN0000000224271061', 'Orthophotographie', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.84501568475336 43.6040131918116)', 'T'),
(143, 'PAISCIEN0000000224271062', 'Orthophotographie', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.8464177687841 43.6005973716316)', 'T'),
(144, 'PAISCIEN0000000224271063', 'Orthophotographie', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.84642172182795 43.6000259578623)', 'T'),
(145, 'PAISCIEN0000000044348652', 'Orthophotographie', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.88098148949953 43.6036501518588)', 'T'),
(146, 'PAISCIEN0000000224271069', 'Orthophotographie', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.88208562421359 43.5979867221585)', 'T'),
(147, 'PAISCIEN0000000224271070', 'Orthophotographie', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.88285709519885 43.5977528017905)', 'T'),
(148, 'PAISCIEN0000000224271074', 'Orthophotographie', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.86624935487735 43.6112344286099)', 'T'),
(149, 'PAISCIEN0000000224271082', 'Orthophotographie', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.86924185782326 43.6001508227016)', 'T'),
(150, 'PAISCIEN0000000224271080', 'Orthophotographie', 'Enseignement secondaire', 'NR', 'NR', 'POINT(3.87329614079204 43.6024614416302)', 'T'),
(151, 'PAISCIEN0000000224271081', 'Orthophotographie', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.86878196786272 43.6002795757794)', 'T'),
(152, 'PAISCIEN0000000224271083', 'Orthophotographie', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.86857766855105 43.6004431765163)', 'T'),
(153, 'PAISCIEN0000000224271093', 'Orthophotographie', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.85415214505822 43.6107920919004)', 'T'),
(154, 'PAISCIEN0000000224271092', 'Orthophotographie', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.85316005130331 43.6110329394862)', 'T'),
(155, 'PAISCIEN0000000224271096', 'Orthophotographie', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.85511532041261 43.5994719830393)', 'T'),
(156, 'PAISCIEN0000000224271097', 'Orthophotographie', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.85468252509693 43.5995095814802)', 'T'),
(157, 'PAISCIEN0000000224271099', 'Orthophotographie', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.83661495845244 43.6084009249984)', 'T'),
(158, 'PAISCIEN0000000224271100', 'Orthophotographie', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.83733541199781 43.6083827783677)', 'T'),
(159, 'PAISCIEN0000000211416643', 'Orthophotographie', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.90730773055215 43.6044926046953)', 'T'),
(160, 'PAISCIEN0000000044348698', 'Orthophotographie', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.84600189830324 43.6005241262181)', 'T'),
(161, 'PAISCIEN0000000044348642', 'Orthophotographie', 'Enseignement secondaire', 'NR', 'NR', 'POINT(3.88386256230225 43.6030652649484)', 'T'),
(162, 'PAISCIEN0000000044348640', 'Orthophotographie', 'Enseignement secondaire', 'NR', 'NR', 'POINT(3.85088956684502 43.6048212805668)', 'T'),
(163, 'PAISCIEN0000000044348629', 'Orthophotographie', 'Enseignement secondaire', 'NR', 'NR', 'POINT(3.88331571790774 43.602451534311)', 'T'),
(164, 'PAISCIEN0000000211416642', 'Orthophotographie', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.90732146532954 43.6041082643546)', 'T'),
(165, 'PAISCIEN0000000044348691', 'Orthophotographie', 'Enseignement sup?rieur', 'NR', 'NR', 'POINT(3.87459223116342 43.5988652363365)', 'T'),
(166, 'PAISCIEN0000000044348688', 'BDTopo', 'Enseignement secondaire', 'NR', 'NR', 'POINT(3.88468534219142 43.5974013978021)', 'T'),
(167, 'PAISCIEN0000000044348686', 'BDTopo', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.86861136818165 43.5996429686516)', 'T'),
(168, 'PAISCIEN0000000044348677', 'Orthophotographie', 'Enseignement secondaire', 'NR', 'NR', 'POINT(3.89128091832832 43.5972494101249)', 'T'),
(169, 'PAISCIEN0000000044348620', 'Orthophotographie', 'Enseignement sup?rieur', 'NR', 'NR', 'POINT(3.84389968018177 43.6171241695014)', 'T'),
(170, 'PAISCIEN0000000044348617', 'Orthophotographie', 'Enseignement secondaire', 'NR', 'NR', 'POINT(3.821683434559 43.6167338156405)', 'T'),
(171, 'PAISCIEN0000000044348609', 'Orthophotographie', 'Enseignement secondaire', 'NR', 'NR', 'POINT(3.8301853523528 43.6169517488234)', 'T'),
(172, 'PAISCIEN0000000044348600', 'Orthophotographie', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.87422907227609 43.6169636169217)', 'T'),
(173, 'PAISCIEN0000000044348597', 'Orthophotographie', 'Enseignement sup?rieur', 'NR', 'NR', 'POINT(3.85726832667725 43.6177780955398)', 'T'),
(174, 'PAISCIEN0000000044348588', 'Orthophotographie', 'Enseignement secondaire', 'NR', 'NR', 'POINT(3.84792608382511 43.6180403476205)', 'T'),
(175, 'PAISCIEN0000000044348567', 'Orthophotographie', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.89210797778766 43.6172394491008)', 'T'),
(176, 'PAISCIEN0000000044348579', 'BDTopo', 'Enseignement sup?rieur', 'NR', 'NR', 'POINT(3.87203068481005 43.6156612297379)', 'T'),
(177, 'PAISCIEN0000000044348671', 'Orthophotographie', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.83199339842878 43.6127528461283)', 'T'),
(178, 'PAISCIEN0000000044348668', 'Orthophotographie', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.82772515988971 43.6136573664753)', 'T'),
(179, 'PAISCIEN0000000044348662', 'Orthophotographie', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.87507451945672 43.6123856859068)', 'T'),
(180, 'PAISCIEN0000000044348659', 'Orthophotographie', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.87966850084569 43.613798218638)', 'T'),
(181, 'PAISCIEN0000000044348646', 'Orthophotographie', 'Enseignement sup?rieur', 'NR', 'NR', 'POINT(3.87338097831386 43.6132415476672)', 'T'),
(182, 'PAISCIEN0000000044348645', 'Orthophotographie', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.86737020079254 43.6139429623395)', 'T'),
(183, 'PAISCIEN0000000044348592', 'Orthophotographie', 'Enseignement secondaire', 'NR', 'NR', 'POINT(3.88001047658341 43.6154232401406)', 'T'),
(184, 'PAISCIEN0000000044348577', 'Orthophotographie', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.87847772447764 43.6153762519576)', 'T'),
(185, 'PAISCIEN0000000044348701', 'Orthophotographie', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.85648120701847 43.584802389253)', 'T'),
(186, 'PAISCIEN0000000044348657', 'BDTopo', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.87118273089096 43.6095726064596)', 'T'),
(187, 'PAISCIEN0000000044348655', 'Orthophotographie', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.86831529845191 43.6101066569622)', 'T'),
(188, 'PAISCIEN0000000044348644', 'Orthophotographie', 'Enseignement secondaire', 'NR', 'NR', 'POINT(3.84863029031614 43.6120600127095)', 'T'),
(189, 'PAISCIEN0000000044348634', 'BDTopo', 'Enseignement secondaire', 'NR', 'NR', 'POINT(3.87157980479439 43.6087280866901)', 'T'),
(190, 'PAISCIEN0000000044348627', 'Orthophotographie', 'Enseignement secondaire', 'NR', 'NR', 'POINT(3.89293724828356 43.6110769011715)', 'T'),
(191, 'PAISCIEN0000000044348643', 'BDTopo', 'Enseignement secondaire', 'NR', 'NR', 'POINT(3.86854088986937 43.6092203269214)', 'T'),
(192, 'PAISCIEN0000000044348647', 'Orthophotographie', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.86965788699421 43.6060332181321)', 'T'),
(193, 'PAISCIEN0000000044348638', 'Orthophotographie', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.86205179517615 43.6061685117425)', 'T'),
(194, 'PAISCIEN0000000044348636', 'Orthophotographie', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.88126269124476 43.6072858568892)', 'T'),
(195, 'PAISCIEN0000000044348632', 'Orthophotographie', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.88625527589257 43.6050172871116)', 'T'),
(196, 'PAISCIEN0000000044348606', 'Orthophotographie', 'Enseignement secondaire', 'NR', 'NR', 'POINT(3.83338700867092 43.6260775878549)', 'T'),
(197, 'PAISCIEN0000000044348604', 'BDTopo', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.81797620448637 43.6236598361345)', 'T'),
(198, 'PAISCIEN0000000044348612', 'BDTopo', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.81410867517719 43.6252032808984)', 'T'),
(199, 'PAISCIEN0000000201322198', 'Orthophotographie', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.8150395976887 43.6251621110925)', 'T'),
(200, 'PAISCIEN0000000201322199', 'Orthophotographie', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.81796857756107 43.6243410624904)', 'T'),
(201, 'PAISCIEN0000000044348614', 'Orthophotographie', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.8437237287444 43.6230976913547)', 'T'),
(202, 'PAISCIEN0000000044348608', 'Orthophotographie', 'Enseignement secondaire', 'NR', 'NR', 'POINT(3.83175656034552 43.6242643227339)', 'T'),
(203, 'PAISCIEN0000000044348602', 'Orthophotographie', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.87412439828511 43.6231804937653)', 'T'),
(204, 'PAISCIEN0000000044348618', 'Orthophotographie', 'Enseignement sup?rieur', 'NR', 'NR', 'POINT(3.83269585832293 43.6220714483911)', 'T'),
(205, 'PAISCIEN0000000044348596', 'Orthophotographie', 'Enseignement sup?rieur', 'NR', 'NR', 'POINT(3.8680103533556 43.6200728831785)', 'T'),
(206, 'PAISCIEN0000000044348582', 'Orthophotographie', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.88307582641349 43.6195294989952)', 'T'),
(207, 'PAISCIEN0000000044348581', 'Orthophotographie', 'Enseignement sup?rieur', 'NR', 'NR', 'POINT(3.86202794758381 43.621724893738)', 'T'),
(208, 'PAISCIEN0000000044348580', 'Orthophotographie', 'Enseignement sup?rieur', 'NR', 'NR', 'POINT(3.86827515969185 43.620874316963)', 'T'),
(209, 'PAISCIEN0000000220713590', 'Orthophotographie', 'Enseignement secondaire', 'NR', 'NR', 'POINT(3.86992322429311 43.6211652815766)', 'T'),
(210, 'PAISCIEN0000000220102817', 'Orthophotographie', 'Enseignement sup?rieur', 'NR', 'NR', 'POINT(3.82357877816809 43.6365175105132)', 'T'),
(211, 'PAISCIEN0000000044348556', 'Orthophotographie', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.81722999878485 43.6409242247009)', 'T'),
(212, 'PAISCIEN0000000044348530', 'Orthophotographie', 'Enseignement secondaire', 'NR', 'NR', 'POINT(3.86425965106814 43.6457085619432)', 'T'),
(213, 'PAISCIEN0000000208927468', 'Orthophotographie', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.82751843573385 43.6342704010167)', 'T'),
(214, 'PAISCIEN0000000044348555', 'Orthophotographie', 'Enseignement secondaire', 'NR', 'NR', 'POINT(3.81708599354992 43.6426439969494)', 'T'),
(215, 'PAISCIEN0000000208927469', 'Orthophotographie', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.83097355585591 43.6341720590293)', 'T'),
(216, 'PAISCIEN0000000044348549', 'Orthophotographie', 'Enseignement sup?rieur', 'NR', 'NR', 'POINT(3.8539582928923 43.6407832271055)', 'T'),
(217, 'PAISCIEN0000000044348548', 'Orthophotographie', 'Enseignement sup?rieur', 'NR', 'NR', 'POINT(3.85676917860055 43.6366020500124)', 'T'),
(218, 'PAISCIEN0000000044348532', 'Orthophotographie', 'Enseignement sup?rieur', 'NR', 'NR', 'POINT(3.86327104815517 43.6477824623623)', 'T'),
(219, 'PAISCIEN0000000044348693', 'Orthophotographie', 'Enseignement secondaire', 'NR', 'NR', 'POINT(3.86671806024005 43.5901236502832)', 'T'),
(220, 'PAISCIEN0000000044348692', 'Orthophotographie', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.8637119354391 43.5952819266906)', 'T'),
(221, 'PAISCIEN0000000044348689', 'Orthophotographie', 'Enseignement secondaire', 'NR', 'NR', 'POINT(3.84761096362654 43.597132780397)', 'T'),
(222, 'PAISCIEN0000000044348685', 'BDTopo', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.86551743707898 43.5941589861501)', 'T'),
(223, 'PAISCIEN0000000044348558', 'Orthophotographie', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.81210109740257 43.6397523753668)', 'T'),
(224, 'PAISCIEN0000000044348547', 'Orthophotographie', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.87385602560424 43.6361358079992)', 'T'),
(225, 'PAISCIEN0000000044348680', 'Orthophotographie', 'Enseignement sup?rieur', 'NR', 'NR', 'POINT(3.89418722094809 43.5906829540563)', 'T'),
(226, 'PAISCIEN0000000220178337', 'Orthophotographie', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.83963303353889 43.5965693586972)', 'T'),
(227, 'PAISCIEN0000000044348590', 'BDTopo', 'Enseignement primaire', 'NR', 'NR', 'POINT(3.8798209667206 43.6261624757751)', 'T'),
(228, 'PAISCIEN0000000044348615', 'Orthophotographie', 'Enseignement secondaire', 'NR', 'NR', 'POINT(3.82372944374482 43.6272543941)', 'T'),
(229, 'PAISCIEN0000000044348605', 'Orthophotographie', 'Enseignement secondaire', 'NR', 'NR', 'POINT(3.82334412104218 43.6283713027754)', 'T'),
(230, 'PAISCIEN0000000044348586', 'Orthophotographie', 'Enseignement secondaire', 'NR', 'NR', 'POINT(3.86500510796669 43.6268644812719)', 'T'),
(231, 'PAISCIEN0000000044348560', 'Orthophotographie', 'Enseignement secondaire', 'NR', 'NR', 'POINT(3.82134401599076 43.6332518225815)', 'T'),
(232, 'PAISCIEN0000000044348550', 'Orthophotographie', 'Enseignement sup?rieur', 'NR', 'NR', 'POINT(3.86572533897883 43.6323153536519)', 'T');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `etablissement`
--
ALTER TABLE `etablissement`
  ADD PRIMARY KEY (`gid`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `etablissement`
--
ALTER TABLE `etablissement`
  MODIFY `gid` int(11) NOT NULL AUTO_INCREMENT COMMENT 'TRIAL', AUTO_INCREMENT=233;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
