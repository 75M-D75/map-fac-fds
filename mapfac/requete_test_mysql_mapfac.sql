SELECT ST_AsGeoJSON(ST_GeomFromText(geom)) FROM quartier_m where nom_iris="Bologne";


select ST_AsGeoJSON(ST_GeomFromText(geom)), id from (select ST_ContainsProperly(q.geom, ST_Boundary(b.geom)) as contains, b.geom, b.id from batis_m b, quartier_m q where nom_iris="Bologne") as tab_contains where contains='t';

select ST_AsGeoJSON(ST_GeomFromText(geom)), id from (select ST_Contains(ST_GeomFromText(q.geom), ST_GeomFromText(b.geom)) as contains, b.geom, b.id from batis_m b, quartier_m q where nom_iris="Bologne") as tab_contains where contains='t'

select ST_AsGeoJSON(ST_GeomFromText(geom)), id, contains from (select ST_Contains(ST_GeomFromText(q.geom), ST_GeomFromText(b.geom)) as contains, b.geom, b.id from batis_m b, quartier_m q where nom_iris="Bologne") as tab_contains where contains != 0

select ST_AsGeoJSON(ST_GeomFromText(geom)), id, contains from (select ST_Contains(ST_GeomFromText(q.geom), ST_GeomFromText(b.geom)) as contains, b.geom, b.id from batis_m b, quartier_m q where nom_iris="Bologne") as tab_contains where contains != 0


CREATE TABLE `batis_m` (`id` VARCHAR(50) NOT NULL COMMENT 'TRIAL',
`geom` TEXT  COMMENT 'TRIAL',
`surface` DOUBLE COMMENT 'TRIAL',
`prix_appt` BIGINT COMMENT 'TRIAL',
`trial529` CHAR(1) COMMENT 'TRIAL',
PRIMARY KEY(`id`)) COMMENT='TRIAL'

SELECT ST_AsGeoJSON(ST_GeomFromText(geom)) FROM batis_m ;


SELECT b.geom , qt.geom, id FROM batis_m b, (SELECT geom from quartier_m q where nom_iris="Bologne" ) qt

select ST_AsGeoJSON(ST_GeomFromText(geom)), id from (select ST_Contains(ST_Boundary(ST_GeomFromText(q.geom)), ST_Boundary(ST_GeomFromText(b.geom))) as contains, b.geom, b.id from batis_m b, quartier_m q where nom_iris="Bologne") as tab_contains where contains='t'


select * from (SELECT ST_AsGeoJSON(ST_GeomFromText(geom)) as bb FROM batis_m) b where bb is not null



select ST_AsGeoJSON(ST_GeomFromText(geom)), id from batis_m where id="BATIMENT0000000101217492" or id = "BATIMENT0000000044584280" ;

select bt.id_btm, synonyme
from batiments bt, batis_m b
where bt.id_btm = b.id_btm and INSTR( synonyme, 'BU' ) != 0

select synonyme 
from batiments bt, batis_m b
where bt.id_btm = b.id_btm and bt.id_btm = 1


select ST_AsGeoJSON(ST_GeomFromText(geom)), id from (select bt.id_btm, synonyme
from batiments bt, batis_m b
where bt.id_btm = b.id_btm and INSTR( synonyme, 'BU' ) != 0) as btm, batis_m b where btm.id_btm = b.id_btm


select ST_AsGeoJSON(ST_GeomFromText(geom)), id, btm.id_btm, btm.synonyme from (select bt.id_btm, synonyme
from batiments bt, batis_m b
where bt.id_btm = b.id_btm) as btm, (select ST_Contains(ST_GeomFromText(q.geom), ST_GeomFromText(b.geom)) as contains, b.geom, b.id from batis_m b, quartier_m q where nom_iris="Vert Bois") as tab_contains where contains != 0 and btm.id_btm = b.id_btm



SELECT ST_AsGeoJSON(ST_GeomFromText(geom)), tab_contains.id, btm.id_btm, btm.synonyme from 
	(select bt.id_btm, synonyme, id
	from batiments bt, batis_m b
	where bt.id_btm = b.id_btm) as btm, 
	
	(select ST_Contains(ST_GeomFromText(q.geom), ST_GeomFromText(b.geom)) as contains, b.geom, b.id 
		from batis_m b, quartier_m q 
		where nom_iris="Vert Bois") as tab_contains 
where contains != 0 and btm.id = tab_contains.id