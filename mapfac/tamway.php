<?php
	$user = "root";
	$pass = "";

	$dbh = new PDO('mysql:host=localhost;dbname=bdd', $user, $pass);

	$t = array();
	$indice_equipement = 0;
	if( array_key_exists('nom_equipement', $_POST) ){
		$nom_equipement = $_POST['nom_equipement'];

		try {
		    $q = 'SELECT ST_AsGeoJSON(ST_GeomFromText(geom)) FROM ' . $nom_equipement;
		    $stmt = $dbh->prepare($q);
			$stmt->execute();
			$ligne = $stmt->fetch(PDO::FETCH_ASSOC);
	        while( $ligne = $stmt->fetch(PDO::FETCH_ASSOC) ){
				foreach ($ligne as $col_value) {
			        //$tab_equipement[$indice_equipement] = $col_value;
					$t['geom'][$indice_equipement] = $col_value;
			        $indice_equipement++;
			    }
			}

			echo json_encode($t, JSON_NUMERIC_CHECK);
			$stmt->closeCursor();
			
		} 
		catch (PDOException $e) {
		    print "Erreur !: " . $e->getMessage() . "<br/>";
		    die();
		}
	}

	$dbh = null;
?>