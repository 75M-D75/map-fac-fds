<?php
	$user = "root";
	$pass = "";

	$dbh = new PDO('mysql:host=localhost;dbname=bdd', $user, $pass);
	if( array_key_exists('nom_quartier', $_POST) ){
		$nom_quartier = $_POST['nom_quartier'];

		$tab_quartier = array();
		$indice_quartier = 0;
		$t = array();
		try {
		    $q = 'SELECT nom_iris FROM quartier_m';
		    
		    $stmt = $dbh->prepare($q);
			$stmt->execute();
			while( $ligne = $stmt->fetch(PDO::FETCH_ASSOC) ){
				foreach ($ligne as $col_value) {
			        $tab_quartier[$indice_quartier] = $col_value;
			        $indice_quartier = $indice_quartier + 1;
			    }
			}

			$stmt->closeCursor();

			$t['nom_quartier'] = $tab_quartier;
			
		} 
		catch (PDOException $e) {
		    print "Erreur !: " . $e->getMessage() . "<br/>";
		    die();
		}
		echo json_encode($t);
	}
	$dbh = null;
?>