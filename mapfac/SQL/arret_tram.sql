-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost
-- Généré le :  jeu. 22 août 2019 à 00:56
-- Version du serveur :  5.7.23
-- Version de PHP :  7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `bdd`
--

-- --------------------------------------------------------

--
-- Structure de la table `arret_tram`
--

CREATE TABLE `arret_tram` (
  `gid` int(11) NOT NULL COMMENT 'TRIAL',
  `nom` varchar(254) DEFAULT NULL COMMENT 'TRIAL',
  `id` smallint(6) DEFAULT NULL COMMENT 'TRIAL',
  `geom` text COMMENT 'TRIAL',
  `trial572` char(1) DEFAULT NULL COMMENT 'TRIAL'
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='TRIAL';

--
-- Déchargement des données de la table `arret_tram`
--

INSERT INTO `arret_tram` (`gid`, `nom`, `id`, `geom`, `trial572`) VALUES
(1, 'AIGUELONGUE', 0, 'MULTIPOINT(3.88321151190695 43.6262278394542)', 'T'),
(2, 'HOP. LAPEYRONIE', 1, 'MULTIPOINT(3.85263979737724 43.6317041193881)', 'T'),
(3, 'UNIVERSITES DES SCIENCES ET LETTRES', 2, 'MULTIPOINT(3.86155395146663 43.6291867384558)', 'T'),
(4, 'MALBOSC', 3, 'MULTIPOINT(3.83318415838832 43.6345766912281)', 'T'),
(5, 'OCCITANIE', 4, 'MULTIPOINT(3.84855180238929 43.6345437877515)', 'T'),
(6, 'CHATEAU D\'O', 5, 'MULTIPOINT(3.84301257938214 43.6316084101497)', 'T'),
(7, 'EUROMEDECINE', 6, 'MULTIPOINT(3.82766497271547 43.6390355077819)', 'T'),
(8, 'HOTEL DU Dï¿½PARTEMENT', 7, 'MULTIPOINT(3.83508844883337 43.6220184756333)', 'T'),
(9, 'SAINT LAZARE', 8, 'MULTIPOINT(3.88869867443633 43.6267091791509)', 'T'),
(10, 'SAINT-ELOI', 9, 'MULTIPOINT(3.86564958821826 43.6269898231064)', 'T'),
(11, 'BOUTONNET', 10, 'MULTIPOINT(3.86806285017101 43.6225778562292)', 'T'),
(12, 'BOUTONNET', 11, 'MULTIPOINT(3.8681813226159 43.6226102415641)', 'T'),
(13, 'HALLES DE LA PAILLADE', 12, 'MULTIPOINT(3.81747591388661 43.6275640062292)', 'T'),
(14, 'STADE DE LA MOSSON', 13, 'MULTIPOINT(3.81735003444225 43.6212459181261)', 'T'),
(15, 'HAUTS DE MASSANE', 14, 'MULTIPOINT(3.82262349003965 43.6363551376426)', 'T'),
(16, 'SAINT PAUL', 15, 'MULTIPOINT(3.82109075286589 43.6306294077612)', 'T'),
(17, 'PILORY', 16, 'MULTIPOINT(3.83203000629587 43.6195608707936)', 'T'),
(18, 'CELLENEUVE', 17, 'MULTIPOINT(3.82587541241159 43.6158768944687)', 'T'),
(19, 'MOSSON', 18, 'MULTIPOINT(3.81965530027248 43.6162573099886)', 'T'),
(20, 'PERGOLA', 19, 'MULTIPOINT(3.83965437920133 43.6176249061543)', 'T'),
(21, 'TONNELLES', 20, 'MULTIPOINT(3.83951413920444 43.6134795155593)', 'T'),
(22, 'JULES GUESDE', 21, 'MULTIPOINT(3.84676126093407 43.6113289773043)', 'T'),
(23, 'ASTRUC', 22, 'MULTIPOINT(3.85485489289235 43.6102832985343)', 'T'),
(24, 'MOULARES (HOTEL DE VILLE)', 23, 'MULTIPOINT(3.8955369238255 43.6006055989814)', 'T'),
(25, 'PORT MARIANNE', 24, 'MULTIPOINT(3.89941085863041 43.6015926480795)', 'T'),
(26, 'LEON BLUM', 25, 'MULTIPOINT(3.89019686697452 43.6089345008142)', 'T'),
(27, 'RIVES DU LEZ', 26, 'MULTIPOINT(3.89496403122472 43.6042320561926)', 'T'),
(28, 'MILLENAIRE', 27, 'MULTIPOINT(3.90996314143537 43.6034169197647)', 'T'),
(29, 'PLACE DE L\'EUROPE', 28, 'MULTIPOINT(3.89398082854659 43.6075135374017)', 'T'),
(30, 'PLACE DE FRANCE', 29, 'MULTIPOINT(3.91578469913588 43.6037142116984)', 'T'),
(31, 'ODYSSEUM', 30, 'MULTIPOINT(3.92029950024783 43.6037846822116)', 'T'),
(32, 'GEORGES FRECHE - HOTEL DE VILLE', 31, 'MULTIPOINT(3.89501435524723 43.5995199167451)', 'T'),
(33, 'PABLO PICASSO', 32, 'MULTIPOINT(3.90337995444496 43.5980509697224)', 'T'),
(34, 'POMPIGNANE', 33, 'MULTIPOINT(3.89499790837432 43.6125912682045)', 'T'),
(35, 'RIVES DU LEZ', 34, 'MULTIPOINT(3.89398192826825 43.6040734593074)', 'T'),
(36, 'LA RAUZE', 35, 'MULTIPOINT(3.89600199734048 43.5935183759427)', 'T'),
(37, 'MONDIAL 98', 36, 'MULTIPOINT(3.90406174631479 43.6026884132444)', 'T'),
(38, 'Pont de Sï¿½te', 37, 'MULTIPOINT(3.8787238425882 43.6040584350239)', 'T'),
(39, 'Saint-Denis', 38, 'MULTIPOINT(3.87497041154203 43.6052535621521)', 'T'),
(40, 'Polygone', 39, 'MULTIPOINT(3.88613957802795 43.6071865093345)', 'T'),
(41, 'LOUIS BLANC', 40, 'MULTIPOINT(3.87813836816181 43.6146989804702)', 'T'),
(42, 'NOUVEAU SAINT-ROCH', 41, 'MULTIPOINT(3.87573242587162 43.5994050745492)', 'T'),
(43, 'COMEDIE', 42, 'MULTIPOINT(3.87983103038264 43.6083372496154)', 'T'),
(44, 'ANTIGONE', 43, 'MULTIPOINT(3.88667390892526 43.6085805476747)', 'T'),
(45, 'HOTEL DE VILLE', 44, 'MULTIPOINT(3.88325682821049 43.607110490668)', 'T'),
(46, 'PLACE CARNOT', 45, 'MULTIPOINT(3.88421429493173 43.6036629608015)', 'T'),
(47, 'LES ARCEAUX', 46, 'MULTIPOINT(3.86194856149036 43.6099538056015)', 'T'),
(48, 'PLAN CABANES', 47, 'MULTIPOINT(3.86825273286235 43.6084807916481)', 'T'),
(49, 'RONDELET', 48, 'MULTIPOINT(3.87605685746492 43.6030418987592)', 'T'),
(50, 'BEAUX ARTS', 49, 'MULTIPOINT(3.88354730113537 43.6169636825076)', 'T'),
(51, 'VOLTAIRE', 50, 'MULTIPOINT(3.88911878195127 43.6037624941663)', 'T'),
(52, 'JEU DE MAIL DES ABBES', 51, 'MULTIPOINT(3.88399592161431 43.6204011844301)', 'T'),
(53, 'GARE SAINT-ROCH', 52, 'MULTIPOINT(3.88012398652076 43.6056918429386)', 'T'),
(54, 'CORUM', 53, 'MULTIPOINT(3.88180288894431 43.6141874535245)', 'T'),
(55, 'CORUM', 54, 'MULTIPOINT(3.88203164247324 43.61449782336)', 'T'),
(56, 'GARE SAINT-ROCH', 55, 'MULTIPOINT(3.87973215441009 43.6052216181867)', 'T'),
(57, 'SAINT DENIS', 56, 'MULTIPOINT(3.87419644360875 43.6055040853603)', 'T'),
(58, 'LES AUBES', 57, 'MULTIPOINT(3.88822891517206 43.6139165185126)', 'T'),
(59, 'GARE SAINT-ROCH', 58, 'MULTIPOINT(3.88006396078673 43.6045329001506)', 'T'),
(60, 'SAINT DENIS', 59, 'MULTIPOINT(3.87264589783825 43.6062790655505)', 'T'),
(61, 'STADE PHILIPPIDES', 60, 'MULTIPOINT(3.86944074676127 43.6189968211492)', 'T'),
(62, 'ALBERT 1ER', 61, 'MULTIPOINT(3.8740725552056 43.6165118009923)', 'T'),
(63, 'ALBERT 1ER CATHEDRALE', 62, 'MULTIPOINT(3.8738789919549 43.6148406166926)', 'T'),
(64, 'SAINT GUILHEM COURREAU', 63, 'MULTIPOINT(3.87348083850767 43.6082499663285)', 'T'),
(65, 'PEYROU ARC DE TRIOMPHE', 64, 'MULTIPOINT(3.87226391293505 43.6114748363101)', 'T'),
(66, 'OBSERVATOIRE', 65, 'MULTIPOINT(3.87664494832987 43.6064312273203)', 'T'),
(67, 'VIL. D\'ANGOULEME', 66, 'MULTIPOINT(3.86560840687606 43.588783328043)', 'T'),
(68, 'LEMASSON', 67, 'MULTIPOINT(3.87306869790136 43.5936180101902)', 'T'),
(69, 'CROIX D\'ARGENT', 68, 'MULTIPOINT(3.86638903086334 43.5923970958974)', 'T'),
(70, 'SABINES', 69, 'MULTIPOINT(3.86009110378584 43.5837538671414)', 'T'),
(71, 'MAS DREVON', 70, 'MULTIPOINT(3.86780524561339 43.5955328489528)', 'T'),
(72, 'SAINT CLEOPHAS', 71, 'MULTIPOINT(3.87602466675032 43.594897678307)', 'T'),
(73, 'SAINT-MARTIN', 72, 'MULTIPOINT(3.87989000642667 43.5925258522164)', 'T'),
(74, 'RESTANQUE', 73, 'MULTIPOINT(3.88605920126618 43.5900623287742)', 'T'),
(75, 'GARCIA LORCA', 74, 'MULTIPOINT(3.89071579767605 43.5909850860346)', 'T');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `arret_tram`
--
ALTER TABLE `arret_tram`
  ADD PRIMARY KEY (`gid`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `arret_tram`
--
ALTER TABLE `arret_tram`
  MODIFY `gid` int(11) NOT NULL AUTO_INCREMENT COMMENT 'TRIAL', AUTO_INCREMENT=76;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
