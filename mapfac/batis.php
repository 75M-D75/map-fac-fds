<?php
	$user = "root";
	$pass = "";

	$dbh = new PDO('mysql:host=localhost;dbname=bdd', $user, $pass);
	if( array_key_exists('nom_quartier', $_POST) ){
		$nom_quartier = $_POST['nom_quartier'];

		$tab_quartier = array();
		$indice_quartier = 0;
		$t = array();
		try {
		    $q = 'SELECT ST_AsGeoJSON(ST_GeomFromText(geom)), tab_contains.id, btm.id_btm, btm.synonyme from (SELECT bt.id_btm, synonyme, id
			from batiments bt, batis_m b
			where bt.id_btm = b.id_btm) as btm, 
			(SELECT ST_Contains(ST_GeomFromText(q.geom), ST_GeomFromText(b.geom)) as contains, b.geom, b.id 
				from batis_m b, quartier_m q 
				WHERE nom_iris="'.$nom_quartier.'") as tab_contains 
			WHERE contains != 0 and btm.id = tab_contains.id;';
			//echo $nom_quartier;
		    $stmt = $dbh->prepare($q);
			$stmt->execute();
			while( $ligne = $stmt->fetch(PDO::FETCH_ASSOC) ){
				//print_r($ligne);
		        $indice_col = 0;
			    foreach ($ligne as $col_value) {
			    	#echo $col_value;
			        $tab_quartier[$indice_quartier][$indice_col] = $col_value;
			        $indice_col = $indice_col + 1;
			    }
			    $indice_quartier = $indice_quartier + 1;
			}

			$t['geom'] = $tab_quartier;

			$stmt->closeCursor();

		} 
		catch (PDOException $e) {
		    print "Erreur !: " . $e->getMessage() . "<br/>";
		    die();
		}
		echo json_encode($t);
	}
	$dbh = null;
?>

