<?php
	$user = "root";
	$pass = "";

	$dbh = new PDO('mysql:host=localhost;dbname=bdd', $user, $pass);
	if( array_key_exists('nom_batiment', $_POST) ){
		$nom_quartier = $_POST['nom_batiment'];

		$tab_quartier = array();
		$indice_bat = 0;
		$t = array();
		try {
		    $q = 'SELECT * from batiments';
			//echo $nom_quartier;
		    $stmt = $dbh->prepare($q);
			$stmt->execute();
			while( $ligne = $stmt->fetch(PDO::FETCH_ASSOC) ){
				//print_r($ligne);
		        $indice_col = 0;
			    foreach ($ligne as $col_value) {
			    	#echo $col_value;
			        $tab_quartier[$indice_bat][$indice_col] = $col_value;
			        $indice_col = $indice_col + 1;
			    }
			    $indice_bat = $indice_bat + 1;
			}

			$t['batiments'] = $tab_quartier;

			$stmt->closeCursor();

		} 
		catch (PDOException $e) {
		    print "Erreur !: " . $e->getMessage() . "<br/>";
		    die();
		}
		echo json_encode($t);
	}
	$dbh = null;
?>