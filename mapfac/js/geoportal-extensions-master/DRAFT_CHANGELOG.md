**TODO : PREPARATION POUR PROCHAINE LIVRAISON**

# Extension Geoportail OpenLayers, version 3.0.6

**22/11/2019**
> Release Extension Geoportail OpenLayers

## Summary

* fix.

## Changelog

* [Added]

* [Changed]

* [Deprecated]

* [Removed]

* [Fixed]

    - Correctif sur le widget Drawing

* [Security]

---

# Extension Geoportail Itowns, version 2.2.5

**22/11/2019**
> Release Extension Geoportail Itowns

## Summary

## Changelog

* [Added]

* [Changed]

* [Deprecated]

* [Removed]

* [Fixed]

* [Security]

---

# Extension Geoportail Leaflet, version 2.1.6

**22/11/2019**
> Release Extension Geoportail Leaflet

## Summary

## Changelog

* [Added]

* [Changed]

* [Deprecated]

* [Removed]

* [Fixed]

* [Security]
