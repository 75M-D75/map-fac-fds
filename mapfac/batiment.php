<?php
	$user = "root";
	$pass = "";

	$dbh = new PDO('mysql:host=localhost;dbname=bdd', $user, $pass);
	if( array_key_exists('synonyme', $_POST) ){
		$synonyme = $_POST['synonyme'];

		$tab_quartier = array();
		$indice_quartier = 0;
		$t = array();
		try {
		    $q = 'SELECT ST_AsGeoJSON(ST_GeomFromText(geom)), id from (select bt.id_btm, synonyme
			from batiments bt, batis_m b
			where bt.id_btm = b.id_btm and INSTR( synonyme, 'BU' ) != 0) as btm, batis_m b where btm.id_btm = b.id_btm';
		    
		    $stmt = $dbh->prepare($q);
			$stmt->execute();
			while( $ligne = $stmt->fetch(PDO::FETCH_ASSOC) ){
				#echo $ligne;
		        $indice_col = 0;
			    foreach ($ligne as $col_value) {
			    	#echo $col_value;
			        $tab_quartier[$indice_quartier][$indice_col] = $col_value;
			        $indice_col = $indice_col + 1;
			    }
			    $indice_quartier = $indice_quartier + 1;
			}

			$stmt->closeCursor();

			$t['batiments'] = $tab_quartier;
			
		} 
		catch (PDOException $e) {
		    print "Erreur !: " . $e->getMessage() . "<br/>";
		    die();
		}
		echo json_encode($t);
	}
	else if( array_key_exists('id_btm', $_POST) ){
		$id_btm = $_POST['id_btm'];

		$tab_quartier = array();
		$indice_quartier = 0;
		$t = array();
		try {
		    $q = 'SELECT ST_AsGeoJSON(ST_GeomFromText(geom)), id from (select bt.id_btm, synonyme
			from batiments bt, batis_m b
			where bt.id_btm = b.id_btm and INSTR( synonyme, 'BU' ) != 0) as btm, batis_m b where btm.id_btm = b.id_btm';
		    
		    $stmt = $dbh->prepare($q);
			$stmt->execute();
			while( $ligne = $stmt->fetch(PDO::FETCH_ASSOC) ){
				#echo $ligne;
		        $indice_col = 0;
			    foreach ($ligne as $col_value) {
			    	#echo $col_value;
			        $tab_quartier[$indice_quartier][$indice_col] = $col_value;
			        $indice_col = $indice_col + 1;
			    }
			    $indice_quartier = $indice_quartier + 1;
			}

			$stmt->closeCursor();

			$t['batiments'] = $tab_quartier;
			
		} 
		catch (PDOException $e) {
		    print "Erreur !: " . $e->getMessage() . "<br/>";
		    die();
		}
		echo json_encode($t);
	}
	$dbh = null;
?>