<!DOCTYPE html>
<html lang="fr">

<!--  PHP : initialise la page selon une requete GET url  -->
<?php
if( array_key_exists('quartier', $_GET) ){
        # Our new data
    echo "
    <script type=\"text/javascript\">
    var inite_name = "."\"".$_GET["quartier"]."\"".";
    </script>
    ";
}
else{
    echo "
    <script type=\"text/javascript\">
    var inite_name = \"Vert Bois\";
    </script>
    ";
}
?>

<head>
    <title>MAP FAC FDS</title>
    <!-- <link rel="stylesheet" href="https://unpkg.com/leaflet@1.0.3/dist/leaflet.css" /> -->
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.6.0/dist/leaflet.css" />
    <script src="https://unpkg.com/leaflet@1.6.0/dist/leaflet.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/side_bar.css"></style>
    <link rel="stylesheet" href="./css/leaflet-panel-layers.css" />

    <script src="./js/leaflet-panel-layers.js"></script>

    <!-- <script src="./lib/require.js" data-main="app"></script> -->
    <link rel="stylesheet" type="text/css" href="./lib/leaflet.css">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    <link rel="stylesheet" href="./dist/L.Control.Locate.min.css" />

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/leaflet.locatecontrol/dist/L.Control.Locate.min.css" />


    <script src="https://cdn.jsdelivr.net/npm/leaflet.locatecontrol/dist/L.Control.Locate.min.js" charset="utf-8"></script>

    <script src="dist/2.7.3/Chart.bundle.js"></script>
    <script src="samples/latest/utils.js"></script>
    <!-- <script type="text/javascript" src="./js/leaflet.wms.js"></script> -->

    <script src="http://ignf.github.io/geoportal-extensions/leaflet-latest/dist/GpPluginLeaflet.js" ></script>
    <!-- <script src="js/openlayers/GpPluginOpenLayers-map.js" charset="utf-8"></script>
        <script src="js/openlayers/GpPluginOpenLayers-src.js" charset="utf-8"></script> -->

        <link rel="stylesheet" href="http://ignf.github.io/geoportal-extensions/leaflet-latest/dist/GpPluginLeaflet-src.css" />
        <script type="text/javascript" src="https://rawgithub.com/mylen/leaflet.TileLayer.WMTS/master/leaflet-tilelayer-wmts.js"></script>

        <script src="https://unpkg.com/leaflet@1.6.0/dist/leaflet-src.js"></script>
        <link rel="stylesheet" href="https://unpkg.com/leaflet@1.6.0/dist/leaflet.css" />
        <script src="https://unpkg.com/leaflet@1.6.0/dist/leaflet.js"></script>


        <!-- Plugin leaflet IGN -->
        <script src="http://ignf.github.io/geoportal-extensions/leaflet-latest/dist/GpPluginLeaflet.js" ></script>
        <link rel="stylesheet" href="http://ignf.github.io/geoportal-extensions/leaflet-latest/dist/GpPluginLeaflet-src.css" />
        <script type="text/javascript" src="https://ignf.github.io/geoportal-access-lib/latest/dist/GpServices.js"></script>
        <script src="./src/L.Control.Locate.js" ></script>

        <style>
            canvas {
                -moz-user-select: none;
                -webkit-user-select: none;
                -ms-user-select: none;
            }
        </style>
    </head>

    <body >
      <div id="map" ></div>
      <div id="result"></div>

      <script>
        var marker_point_dep    = null;
        var marker_point_end    = null;
        var polyline_route_gps  = null;

        var map = L.map('map').setView([43.613425, 3.866070], 12);
        var routeCtrl = null;

        function go() {
            // var lyrMaps = L.geoportalLayer.WMTS({
            //         layer: "GEOGRAPHICALGRIDSYSTEMS.MAPS",
            // }, { // leafletParams
            //     opacity: 0.7
            // }).addTo(map);
            // var layer = L.geoportalLayer.WMTS({
            //      layer : "ORTHOIMAGERY.ORTHOPHOTOS",
            // }).addTo(map) ;


            var routeCtrl = L.geoportalControl.Route({position:"topright"
        });

        //var graph = document.getElementById("graph").options[graphIdx].value;
        //var graphIdx = document.getElementById("graph").selectedIndex;

        function trace_route(point_1, point_2, latlngs){
            marker_point_dep = L.marker([point_1.y, point_1.x]).addTo(map);
            marker_point_end = L.marker([point_2.y, point_2.x]).addTo(map);

            for (var i = 0; i < latlngs.length; i++) {
                latlngs[i] = latlngs[i].reverse();
            }
            polyline_route_gps = L.polyline(latlngs, {color: 'red'}).addTo(map);

        }

        function gps(point_1, point_2){
            Gp.Services.route({
                startPoint: point_1,
                endPoint: point_2,
                graph: "Voiture",
                // avoidFeature: avoidFeatures,
                routePreference: "fastest",
                apiKey: "choisirgeoportail",
                outputFormat : "json",
                onSuccess: function(result) {
                    console.log(result);
                    trace_route(point_1, point_2, result.routeGeometry.coordinates);
                },
                onFailure: function(error) {
                    // resultDiv.innerHTML = "<p>" + error + "</p>";
                    console.log("error ")
                    console.log(error)
                }
            });
        }

        try {
            gps(L.point(3.8642 , 43.633), L.point(3.8663 , 43.6323));
        } 
        catch (e) {
            console.log(e)
        }
        //routeCtrl.compute([{lng:0 , lat:0 }],{position:"topleft"})
        map.addControl(routeCtrl);
        //map.removeControl(routeCtrl)
        
        //map.addControl(routeCtrl2);
        //point = L.point(3.5, 41.6);
        //console.log(point);
    }


    window.onload = function () {

        /*Gp.Services.getConfig({
          apiKey: "choisirgeoportail",
          onSuccess: go
        });*/
        console.log("oko")
        //console.log(Gp.point(3.5,46.5));
    }

    



</script>


<div id="map_img" onclick="show_image_map()">

    <img id="img_map" src="image/map_univ_mtp.jpg">
</div>

<md-toolbar class="md-primary">
</md-toolbar>
<dir id="gauche">

    <div class="champ"> 
        <div class="custom-select" id="custom_select_quartier_id" style="width:200px;"  >
            <select name='nom_quartier' id='quartier' onchange='pour_select_quartier(this.value)'> 
            </select>
        </div>
    </div>

    <div class="champ"> 
        <div class="custom-select" id="custom_select_bat_id" style="width:200px;"  >
            <select name='nom_batiment' id='bat' onchange='pour_select_batiment(this.value)'> 
            </select>
        </div>
    </div>

    <div class="custom-select" style="width:200px;"  >
        <select name='equipement_public' id='equipement_public' onchange='get_equipement_public(this.value)'> 
            <option value="0">Equipement public:</option> 
            <option value="tramway">Tramway</option>
            <option value="arret_tram">Arret Tram</option>
            <option value="loisir">Loisir</option>
            <option value="hopitaux">Hopitaux</option>
            <option value="etablissement">Etablissement</option>
            <option value="transport">Infrastructure transport</option>
            <option value="commercial">Commerciale</option>
        </select>
    </div>

    <!-- <div id="right"></div> -->

    <div>
        <input class="btn" type="button" name="quartier" value="Quartiers" onclick="afficher_tout_quartier()">
    </div>

    <div>
        <input class="btn" type="button" name="position" value="Votre position" onclick="localisation_your_position()">
    </div>

    <div>
        <input class="btn" type="button" name="map_image" value="Image Carte FDS" onclick="show_image_map()">
    </div>

</dir>

<div id="logos"> 
    <div><img class="im_logo" id="logo" src="image/logo_fds.png" alt="Italian Trulli"></div>
</div>

<!-- SIDE BAR -->

<div class="container">
    <aside class="sidebar">
        <nav class="nav">
            <ul class="nav-items">
                <li class="nav-title">Selecteur</li>

                <li>
                    <a href="#" class="nav-link"> 
                        <div class="custom-select" id="custom_select_quartier_id_sb" style="width:200px;"  >
                            <select name='nom_quartier' id='quartier' onchange='pour_select_quartier(this.value)'> 
                            </select>
                        </div>
                    </a>
                </li>

                <li>
                    <a href="#" class="nav-link"> 
                        <div class="custom-select" id="custom_select_bat_id_sb" style="width:200px;"  >
                            <select name='nom_batiment' id='bat_sb' onchange='pour_select_batiment(this.value)'> 
                            </select>
                        </div>
                    </a>
                </li>

                <li>
                    <a href="#" class="nav-link">
                        <div class="custom-select" style="width:200px;"  >
                            <select name='equipement_public' id='equipement_public_sb' onchange='get_equipement_public(this.value)'> 
                                <option value="0">Autres:</option> 
                                <option value="tramway">Tramway</option>
                                <option value="arret_tram">Arret Tram</option>
                                <option value="loisir">Loisir</option>
                                <option value="hopitaux">Hopitaux</option>
                                <option value="etablissement">Etablissement</option>
                                <option value="transport">Infrastructure transport</option>
                                <option value="commercial">Commerciale</option>
                            </select>
                        </div>
                    </a>
                </li>  

            </ul>
            <!-- <div class="sidebar-separator"></div> -->

            <ul class="nav-items">
                <li class="nav-title">Bouton</li>

                <li>
                    <a href="#" class="nav-link">
                        <a class="btn" name="quartier" onclick="afficher_tout_quartier()">Quartiers</a>
                    </a>
                </li>

                <li>
                    <a href="#" class="nav-link">
                        <div>
                            <a class="btn" name="position" onclick="localisation_your_position()">Votre position</a>
                        </div>
                    </a>
                </li>

                <li>
                    <a href="#" class="nav-link">
                        <div>
                           <a class="btn" name="map_image" onclick="show_image_map()">Image Carte FDS</a>
                       </div>
                   </a>
               </li>

           </ul>

       </nav>
   </aside>

   <div class="hamburger">
    <div class="bar"></div>
    <div class="bar"></div>
    <div class="bar"></div>
</div>
</div>

<script type="text/javascript" src="js/side_bar.js"></script>



<script>
    var tableau_ligne_tram          = [];
    var tableau_quartier            = [];
    var tableau_cercle_equipement   = [];
    var tableau_batis               = [];
    var polygone_quartier           = null;
    var retour                      = null;
    var coordX_1                    = null;//
    var coordY_1                    = null;//
    var coordX_2                    = null;//
    var coordY_2                    = null;//
    var ligne                       = null;
    var nom_quartier                = null;//Pour stocket le nom du quartier selectionné
    var color                       = "blue";
    var indice_colore_circles       = 0;//juste pour changer les couleur pas important
    var indice_colore_polygone      = 0;
    var premier_point_distance      = null;
    var distance_activer            = false;
    var nom_batiment                = null;
    var tab_init_select_bool        = [false, false];
    var bati_init                   = false;

    //Inti *****************
    function init(nom){
        //Cpmmence par remplir tout les nom des quartier dans le select
        quartier();
        //get_batis(nom);
        get_geom(nom);
        get_batiment();
        bat_name();
        
    }

    function onLocationFound(e) {
        var radius = e.accuracy;

        L.marker(e.latlng).addTo(map).bindPopup("Vous êtes à moins de " + radius + " mètres de ce point").openPopup();

        L.circle(e.latlng, radius).addTo(map);
    }

    
    var bool_show_img_map = false;
    function show_image_map(){
        if(!bool_show_img_map){
            $("#map_img").css("display", "initial");
            bool_show_img_map = true;
        }
        else{
            $("#map_img").css("display", "none");
            bool_show_img_map = false;
        }
    }
    
    window.onload = function(){

    };

    //permet de changer les couleur des couches
    function change_color(indice_colore){
        switch(indice_colore){
            case 1:
            color = "red";
            break;
            case 2:
            color = "green";
            break;
            case 3:
            color = "gray";
            break;
            case 4:
            color = "yellow";
            break;
            default :
            color = "blue";
        }
        return color;
    }

    /*
     * permet de recuperer le prix foncier et de l'afficher sur l'input dont le name=prix 
     */
     function get_prix_foncier(nom){
        var url = "post.php";//fichier php pour recuperer le prix fonccier

        $.post(url, {nom_quartier:nom}, function(data){

            retour = data.prix_foncier;

            //Change le texte dans le cadre d'affichage du prix foncier
            if(retour != null)
                $("input[name=prix]").val(retour+"€");
            else{
                $("input[name=prix]").val("Aucun prix foncier");
            }


        },"json");
    }

    function del_tout_quartier(){
        if(tableau_quartier.length>1){
            for(var e=0; e<tableau_quartier.length; e++){
                tableau_quartier[e].remove();
            }
            tableau_quartier = [];
        }
    }

    //
    function afficher_tout_quartier(){
        var url = "tout_quartier.php";
        del_tout_quartier();

        $.post(url, {nom_quartier:"nom"}, function(data){
            donne = data.geom;
            console.log(donne.length);
            for(var i=0; i<donne.length; i++){
                //console.log(i);
                k = JSON.parse(donne[i]);
                
                retour = k.coordinates;

                //console.log(retour);

                //inversement des position des coordonnés dans un tableau
                var retour_reverse = [];
                for(var e=0; e<retour[0][0].length; e++){
                    retour_reverse.push(retour[0][0][e].reverse());
                }
                console.log(retour_reverse);
                var objet;
                poly = L.polygon(retour_reverse, {
                    fillOpacity:0.1,
                    color: "red"
                }).addTo(map);

                tableau_quartier.push(poly);
            }

        },"json");
    }

    //de passer en mode distance
    function distance_on(){
        distance_activer = !distance_activer;
        element = document.getElementById("distance_btn");

        if(distance_activer){
            element.style.backgroundColor = "#f44336";
            element.value = "Distance : On"; 
        }
        else{
            element.style.backgroundColor = "#4CAF50";
            element.value = "Distance : Off";
            if(ligne != null){
                ligne.remove();
            }
        }
    }

    /*
     * lorsque l'on change le nom du quartier selectionner
     */
     function pour_select_quartier(nom){
        if( nom != "0" ){
            //recupere le prix foncier
            //get_prix_foncier(nom);
            get_geom(nom);

            //on conserve le nom du quartier
            nom_quartier = nom;
        }
    }

    function pour_select_batiment(nom){
        if( nom != "0" ){
            //recupere le prix foncier
            //get_batiment_selectionner(nom);
            //select_batiment(nom);
            for (var i = 0; i < tableau_batis.length; i++) {
                if(tableau_batis[i].synonyme.includes(nom)){
                    select_batiment(tableau_batis[i]);
                    break;
                }
            }

            //on conserve le nom du quartier
            nom_batiment = nom;
        }
    }

    function get_batiment_selectionner(nom){
        var url = "batiment_select.php";//fichier php pour recuperer le prix fonccier
        
        $.post(url, {nom_batiment:nom}, function(data){

            retour = data.batiments;
            //console.log(retour)
            //Change le texte dans le cadre d'affichage du prix foncier
            

        },"json");
    }

    /*
     * Pour afficher la couche du quartier selectionner
     */
     function afficher_quartier(){
        if( nom_quartier != null )
            get_geom(nom_quartier);

    }

    //arrondir un nombre decimal
    function roundDecimal(nombre, precision){
        var precision = precision || 2;
        var tmp = Math.pow(10, precision);
        return Math.round( nombre*tmp )/tmp;
    }

    //Trait ou pointillé de distance lorsque la souris est en mouvements
    function trace_distance(e){
        e.latlng.lat;
        if(ligne != null)
            ligne.remove();

        ligne = L.polyline([[premier_point_distance.point.lat, premier_point_distance.point.lng], [e.latlng.lat, e.latlng.lng]],{
            color:"black",
            dashArray:"5", 
            weight:1}).addTo(map);
        
        $("input[name=distance]").val(roundDecimal((map.distance(premier_point_distance.point, e.latlng)/1000), 2)+"Km");
    }

    /*
     * Pour recuperer les coordonnées d'un point sur la carte ou l'on a effectuer un clique
     */
     function get_position_marker(e){
        var distance = 0;
        var url = 'distance.php';

        //Si le mode distance est activé
        if(distance_activer){

            //On supprime le point du premier point de distance si il existe
            if(premier_point_distance != null){
                premier_point_distance.remove();
            }

            //lors du premier clique on recupere les coordonnées du premier point
            if( coordX_1 == null ){
                coordX_1 = e.latlng.lat;//latitude
                coordY_1 = e.latlng.lng;//longitude

                premier_point_distance = L.circleMarker([coordX_1, coordY_1], {
                    color: "black",
                    fillColor: "black",
                    weight : 1,
                    fillOpacity: 0.8,
                    radius: 3
                }).addTo(map);

                Object.defineProperty(premier_point_distance, 'point', {
                  value: e.latlng,
                  writable: true
              });

                map.addEventListener("mousemove", trace_distance);
            }
            else{//lors du second clique on recupere les coordonnées du second point
                coordX_2 = e.latlng.lat;
                coordY_2 = e.latlng.lng;
                map.removeEventListener("mousemove", trace_distance);

                //on effectue le calcule de la distance de deux point en interrogant un fichier php (partie serveur)
                $.post(url, {coordX_1:coordX_1, coordY_1:coordY_1, coordX_2:coordX_2, coordY_2:coordY_2}, function(data){
                    retour   = data.distance;
                    distance = retour[0];
                    $("input[name=distance]").val((roundDecimal(distance/1000, 2))+"Km");
                },"json");

                //Efface la ligne precedament afficher sur la carte
                if(ligne != null)
                    ligne.remove();

                //Affiche la ligne qui relie les deux points selectionnée sur la carte
                ligne = L.polyline([[coordX_1, coordY_1], [coordX_2, coordY_2]],{color:"red", 
                    dashArray:"8",
                    weight:1}).addTo(map);
                
                //Réinitialise les points à null
                coordX_1 = null;
                coordY_1 = null;
            }
        }
    }

    function del_batis(){
        for(var i=0; i<tableau_batis.length; i++){
            tableau_batis[i].remove();
            if(tableau_batis[i].marker != null){
                tableau_batis[i].marker.remove();
            }
        }   

        tableau_batis = [];
    }

    function get_min_dist(nom){
        var url = "distance_min.php";

        $.post(url, {nom_quartier:nom}, function(data){

            retour = data.geom;
            config.data.datasets.forEach(function(dataset) {
                dataset.data = retour;
                console.log(retour);
                /*dataset.data = dataset.data.map(function() {
                    return randomScalingFactor();
                });*/
            });

            window.myRadar.update();
            
        },"json");
    }

    function poly_event(e){
        get_batis(e.target.nom);
    }

    /*
     * Recupere les batis
     */
     function get_batis(nom){
        var url = "batis.php";
        console.log("get_batis")
        del_batis();               

        if (coordX_1 == null) {//si il ny'a pas eu un premier click pour recuperer les coordonner d'un point

            //get_min_dist(nom);
            console.log(nom);
            //console.log("bais")
            $.post(url, {nom_quartier:nom}, function(data){

                retour = data.geom;
                //console.log(retour[0]);
                for(var i=0; i<retour.length; i++){
                    //k = JSON.parse(retour[i]);//Convertie la chaine de caractére en format JSON
                    k = retour[i];
                    //console.log(k);
                    var tab = [];
                    kc = JSON.parse(k[0]);//recupere les données geom
                    //kc = k[0];
                    //console.log(kc["coordinates"][0][0]);
                    for(var e=0; e<kc["coordinates"][0][0].length; e++){
                        tab.push([kc["coordinates"][0][0][e][1], kc["coordinates"][0][0][e][0]]);
                    }

                    //console.log(tab);

                    var bati = L.polygon(tab, {
                        color: "#000000",
                        weight: 3
                    }).addTo(map);


                    //console.log(k[1]);

                    Object.defineProperty(bati, 'id', {
                      value: k[1],
                      writable: true
                  });

                    Object.defineProperty(bati, 'coord', {
                      value: tab,
                      writable: true
                  });

                    Object.defineProperty(bati, 'synonyme', {
                      value: k[3],
                      writable: true
                  });

                    bati.addEventListener("click", recupere_id);

                    //TODO
                    tableau_batis.push(bati);
                }

                if(!bati_init){
                    pour_select_batiment(name_batiment);
                    bati_init = true;
                }

                test_dumontet();
                
            },"json");
        }
    }

    var polygon_bat_selectionner    = null;
    var marker_bat_select           = null;

    function recupere_id(e){//affichage sur la console
        select_batiment(e.target);
    }
    //marker_bat_select   = L.marker(latlng).addTo(map).bindPopup(str_popup.join(", ")).openPopup();
    //BATIMENT0000000211128930
    var marker_dumontet = null;
    function test_dumontet(){
        for (var i = 0; i < tableau_batis.length; i++) {
            if(tableau_batis[i].id == "BATIMENT0000000211128930"){
                var poly = L.polygon(tableau_batis[i].coord, {
                    color: "cyan"
                }).addTo(map);
                marker_dumontet  = L.marker(poly.getCenter()).addTo(map).bindPopup("Amphi Dumontet : Batiment 7").openPopup();
                tableau_batis[i].marker = marker_dumontet;
                // var popup = L.popup()
                // .setLatLng(poly.getCenter())
                // .setContent('<p>Hello world!<br />This is a nice popup.</p>')
                // .openOn(map);
                poly.remove();

            }
        }
    }

    

    function select_batiment(objet){
        console.log(objet.id);

        if(polygon_bat_selectionner != null){
            polygon_bat_selectionner.remove();
            polygon_bat_selectionner = null;
        }

        if(marker_bat_select != null){
            marker_bat_select.remove();
            marker_bat_select = null;
        }

        polygon_bat_selectionner = L.polygon(objet.coord, {
            color: "cyan"
        }).addTo(map);

        var latlng = polygon_bat_selectionner.getCenter();
        map.setView(latlng, 18, {animate:true, duration:0.50});

        //var latlng          = polygon_bat_selectionner.getCenter();
        var str_popup       = objet.synonyme.replace("[", "").replace("]", "").split(",");

        marker_bat_select   = L.marker(latlng).addTo(map).bindPopup(str_popup.join(", ")).openPopup();
        console.log(latlng);
        console.log(objet.coord);

    }

    function get_equipement_public(nom){

        var cercleMarker;
        var url = "equipement_public.php";

        if(tableau_ligne_tram.length > 1){
            for (var i = tableau_ligne_tram.length - 1; i >= 0; i--) {
                tableau_ligne_tram[i].remove();
            }
            tableau_ligne_tram = [];
        }

        if( nom != "0"){

            if(tableau_cercle_equipement.length > 1){
                for (var i = tableau_cercle_equipement.length - 1; i >= 0; i--) {
                    tableau_cercle_equipement[i].remove();
                }
                tableau_cercle_equipement = [];
            }

            $.post(url, {nom_equipement:nom}, function(data){
                retour = data.geom;
                var col = change_color(indice_colore_circles++);
                if( nom != "tramway" && nom != "arret_tram" ){

                    //L.marker([51.5, -0.09]).addTo(mymap);
                    for(var i=0; i<retour.length; i++){
                        k = JSON.parse(retour[i]);
                        cercleMarker = L.marker(k["coordinates"].reverse(), {
                            color: col,
                            fillColor: col,
                            weight : 1,
                            fillOpacity: 0.5,
                            radius: 5
                        }).addTo(map);

                        tableau_cercle_equipement.push(cercleMarker);
                        //cercleMarker.addEventListener("click", get_position_marker);
                    }

                }
                else if( nom == "arret_tram" ){

                    for(var i=0; i<retour.length; i++){
                        k = JSON.parse(retour[i]);
                        /* cercleMarker = L.circleMarker(k["coordinates"][0].reverse(), {
                            color: col,
                            fillColor: col,
                            weight : 1,
                            fillOpacity: 0.5,
                            radius: 5
                        }).addTo(map);*/

                        cercleMarker = L.marker(k["coordinates"][0].reverse(), {icon: icon_tram}).addTo(map);

                        tableau_cercle_equipement.push(cercleMarker);
                        //cercleMarker.addEventListener("click", get_position_marker);
                    }
                }
                else{
                    var ligne_tram;

                    for(var i=0; i<retour.length; i++){
                        k = JSON.parse(retour[i]);
                        //alert(k["coordinates"][0]);
                        var retour_reverse = [];
                        for(var a=0; a<k["coordinates"][0].length; a++){
                            retour_reverse.push(k["coordinates"][0][a].reverse());
                        }

                        ligne_tram = L.polyline(retour_reverse, {
                            color: 'blue',
                            fillColor: '#f03',
                            opacity: 1,
                            weight:3,
                            lineJoin: 'round'
                        }).addTo(map);

                        tableau_ligne_tram.push(ligne_tram);
                    }
                }
                
                if(indice_colore_circles>4)
                    indice_colore_circles = 0;

            },"json");
        }
    }

    //Permet de recuperer la couche quartier à partir de la base de données
    
    function get_geom(nom){
        del_tout_quartier();
        del_batis(); 
        var url = "geom.php";
        $.post(url, {nom_quartier:nom}, function(data){
            retour = data.coordinates;

            //inversement des position des coordonnés dans un tableau
            var retour_reverse = [];
            for(var i=0; i<retour[0][0].length; i++){
                retour_reverse.push(retour[0][0][i].reverse());
            }

            //Supprime le polygone du quartier precedement afficher
            if(polygone_quartier != null){
                polygone_quartier.remove();
            }

            //console.log(retour_reverse);
            var objet;
            polygone_quartier = L.polygon(retour_reverse, {
                fillOpacity:0.1,
                color: "#F62D27"
            }).addTo(map);

            //Zoom
            var latlng = polygone_quartier.getCenter();
            map.setView(latlng, 16, {animate:true, duration:0.50});//change la vue centrale de la carte avec zoom et changement de position

            //pour rataché le nom du quartier au polygone
            Object.defineProperty(polygone_quartier, 'nom', {
              value: nom,
              writable: true
          });

            
            polygone_quartier.addEventListener("click", poly_event);

            if(indice_colore_polygone > 4)
                indice_colore_polygone = 0;


        },"json");
        get_batis(nom);
    }

    //Permet de mettre en place le selecteur quartier avec tout les nom de quartier contenue dans la base de données
    function quartier(){
        var url = "quartier.php";
        $.post(url, {nom_quartier:0}, function(data){
            retour = data.nom_quartier;
            
            
            create_select("custom_select_quartier_id", retour, "Select quartier:");
            tab_init_select_bool[0] = true;
            create_select("custom_select_quartier_id_sb", retour, "Select quartier:");
            
        },"json");
    }


    function bat_name(){

        var url = "batiment_fac.php";
        $.post(url, {nom_batiment:0}, function(data){
            retour = data.batiments;
            var donnee = [];
            for (var i = 0; i < retour.length; i++) {
                if( i == 0 ){
                    donnee.push(retour[i][1].replace("[","").replace("]","").split(", ")[4]);
                }
                else{
                    donnee.push(retour[i][1].replace("[","").replace("]","").split(", ")[1]);
                }
            }
            
            create_select("custom_select_bat_id", donnee, "Select batiment:");

            //a mettre pour le derniers chargement
            tab_init_select_bool[1] = true;
            create_select("custom_select_bat_id_sb", donnee, "Select batiment:");

            
            //alert(donnee[0]);
        },"json");
    }


    //Pour cree le selectionneur de quartier
    function create_select(custom_select, donnee, titre){
        var x, i, j, selElmnt, a, b, c;
        /*look for any elements with the class "custom-select":*/
        x = document.getElementById(custom_select);

        selElmnt = x.getElementsByTagName("select")[0];
        //alert(selElmnt.innerHTML);
        selElmnt.innerHTML = "";

        /*for each element, create a new DIV that will act as the selected item:*/
        //Premier option pour le titre du selecteur
        a = document.createElement("OPTION");
        a.setAttribute("value", "0");
        a.innerHTML = titre;
        selElmnt.appendChild(a);

        //ajout des option en fonction des données
        for (j = 0; j<donnee.length; j++) {
            a = document.createElement("OPTION");
            a.setAttribute("value", donnee[j]);
            a.innerHTML = donnee[j];
            selElmnt.appendChild(a);
        }

        x.setAttribute("class", "custom-select");
        style_custom();
    }


    function get_batiment(){
        var url = "batiment_fac.php";
        $.post(url, {nom_batiment:0}, function(data){
            retour = data.batiments;
            //console.log(retour);
            //alert(retour[0][1]);
        },"json");
    }

</script>

<script>

    /* Function que tu as copier coller */
    function style_custom(){

    //veririfie que tout les selection soit bonnes
    var bool = true;
    for (var i = 0; i < tab_init_select_bool.length; i++) {
        if( ! tab_init_select_bool[i] ){
            bool = false;
            break;
        }
    }

    if (!bool) {
        return;
    }

    var x, i, j, selElmnt, a, b, c;
    /*look for any elements with the class "custom-select":*/
    x = document.getElementsByClassName("custom-select");
    for (i = 0; i < x.length; i++) {
        selElmnt = x[i].getElementsByTagName("select")[0];
        /*for each element, create a new DIV that will act as the selected item:*/
        a = document.createElement("DIV");
        a.setAttribute("class", "select-selected");
        a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
        x[i].appendChild(a);
        /*for each element, create a new DIV that will contain the option list:*/
        b = document.createElement("DIV");
        b.setAttribute("class", "select-items select-hide");
        b.setAttribute("id", "id_div_"+selElmnt.id);
        for (j = 1; j < selElmnt.length; j++) {
            /*for each option in the original select element,
            create a new DIV that will act as an option item:*/
            c = document.createElement("DIV");
            c.innerHTML = selElmnt.options[j].innerHTML;

            c.addEventListener("click", function(e) {
                /*when an item is clicked, update the original select box,
                and the selected item:*/
                var y, i, k, s, h;
                s = this.parentNode.parentNode.getElementsByTagName("select")[0];
                h = this.parentNode.previousSibling;

                for (i = 0; i < s.length; i++) {
                    if (s.options[i].innerHTML == this.innerHTML) {
                        s.selectedIndex = i;
                        h.innerHTML = this.innerHTML;
                        y = this.parentNode.getElementsByClassName("same-as-selected");
                        for (k = 0; k < y.length; k++) {
                            y[k].removeAttribute("class");
                        }
                        this.setAttribute("class", "same-as-selected");
                        s.onchange();
                        
                        break;
                    }
                }

                h.click();
            });
            b.appendChild(c);
        }
        x[i].appendChild(b);

        a.addEventListener("click", function(e) {
            /*when the select box is clicked, close any other select boxes,
            and open/close the current select box:*/
            e.stopPropagation();
            closeAllSelect(this);
            this.nextSibling.classList.toggle("select-hide");
            this.classList.toggle("select-arrow-active");
        });
    }

}


function closeAllSelect(elmnt) {
    /*a function that will close all select boxes in the document,
    except the current select box:*/
    var x, y, i, arrNo = [];
    x = document.getElementsByClassName("select-items");
    y = document.getElementsByClassName("select-selected");

    for (i = 0; i < y.length; i++) {
        if (elmnt == y[i]) {
            arrNo.push(i)
        } 
        else {
            y[i].classList.remove("select-arrow-active");
        }
    }

    for (i = 0; i < x.length; i++) {
        if (arrNo.indexOf(i)) {
            x[i].classList.add("select-hide");
        }
    }
}

/*if the user clicks anywhere outside the select box,
then close all select boxes:*/
document.addEventListener("click", closeAllSelect);

//var url = 'montpellier.json';  // my GeoJSON data source, in same folder as my html page.

// var map = L.map('map').setView([43.613425, 3.866070], 12);
//map.addEventListener("click", get_position_marker);
function onLocationError(e) {
    alert(e.message);
}

map.on('locationerror', onLocationError);
//map.on('locationfound', onLocationFound);
var lc = L.control.locate({
    position: 'topright',
    locateOptions: {
        enableHighAccuracy: true
    },
    strings: {
        title: "Votre position"
    }
}).addTo(map);

map.addControl(lc);

var position_localisation = false;

function localisation_your_position(){
    if ( !position_localisation ){
        lc.start();
        position_localisation = true;
    }
    else{
        lc.stop();
        position_localisation = false;
    }
    
}



//map.locate({setView: false, maxZoom: 16});  

var osm=new L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png',{ 
    attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'}).addTo(map);


// Set style function that sets fill color property
function style(feature) {
    return {
        fillColor: 'green', 
        fillOpacity: 0.5,  
        weight: 2,
        opacity: 1,
        color: '#ffffff',
        dashArray: '3'
    };
}

var highlight = {
    'fillColor': 'yellow',
    'weight': 2,
    'opacity': 1
};


</script>


<script type="text/javascript">

    var icon_tram = L.icon({
        iconUrl: 'image/icone-tram.png',

        iconSize:     [35, 35]
    });

    /*function go() {
        //map = L.map("map").setView([47, 2.424], 6);
        var lyrMaps = L.geoportalLayer.WMTS({
            layer: "GEOGRAPHICALGRIDSYSTEMS.MAPS",
        }, { // leafletParams
            opacity: 0.7
        });
        map.addLayer(lyrMaps) ;
        var routeCtrl = L.geoportalControl.Route({
        });
        map.addControl(routeCtrl);
    }

    Gp.Services.getConfig({
        apiKey : "choisirgeoportail",
        onSuccess : go
    }) ;

    jQuery(document).ready(function($){
        var infoDiv = document.getElementById("info") ;
        infoDiv.innerHTML = "<p> Extension Leaflet version "+Gp.leafletExtVersion+" ("+Gp.leafletExtDate+")</p>" ;
    });*/

    /*function go() {

      //map = L.map("map",{crs : L.CRS.EPSG4326}).setView([16.239, -61.545], 12);
      L.geoportalLayer.WMS({
        layer: "OI.OrthoimageCoverage",
      }, { // leafletParams
        opacity: 0.7,
      }).addTo(map);
      L.geoportalLayer.WMS({
        layer: "BU.Building"
      },{
          // on surcharche le style car les styles par defaut pour le WMS INSPIRE vecteur
          // ne sont pas bien renseignes dans l'autoconf (en cours de correction)
          styles : "inspire_common:DEFAULT",
          transparent : true,
      }).addTo(map);
      L.geoportalLayer.WMS({
        layer: "TN.RoadTransportNetwork",
      },{
          styles : "inspire_common:DEFAULT",
          transparent : true,
      }).addTo(map);
      L.geoportalLayer.WMS({
        layer: "HY.PhysicalWaters",
      },{
          styles : "inspire_common:DEFAULT",
          transparent : true,
      }).addTo(map);
      L.geoportalLayer.WMS({
        layer: "GN.GeographicalNames",
      },{
          styles : "inspire_common:DEFAULT",
          transparent : true,
      }).addTo(map);  
    }

    const Parcels = L.tileLayer.WMS('http://services.nconemap.gov/arcgis/services/NC1Map_Parcels/MapServer/WMSServer', {
            layers: 'Parcels',
            format: 'image/png',
            transparent: true
    }).addTo(map);

    Gp.Services.getConfig({
      apiKey: "choisirgeoportail",
      onSuccess: go
  });*/

  window.onload = function () {

    Gp.Services.getConfig({
      apiKey: "choisirgeoportail",
      onSuccess: go
  });

        //alert(inite_name);
        console.log(inite_name);
        init(inite_name);
    }
</script>
<script type="text/javascript" src="https://rawgithub.com/mylen/leaflet.TileLayer.WMTS/master/leaflet-tilelayer-wmts.js">
    console.log("ok")
    var ignKey = "lqp42l06r6pyp1ll2uuzei4r";

        /** Define the layer type
         *  GEOGRAPHICALGRIDSYSTEMS.MAPS
         *  GEOGRAPHICALGRIDSYSTEMS.MAPS.SCAN-EXPRESS.CLASSIQUE
         *  GEOGRAPHICALGRIDSYSTEMS.MAPS.SCAN-EXPRESS.STANDARD
         */
         var layerIGNScanStd = "GEOGRAPHICALGRIDSYSTEMS.MAPS.SCAN-EXPRESS.STANDARD";

        // The WMTS URL 
        var url = "http://wxs.ign.fr/" + "choisirgeoportail" + "/geoportail/wmts";

        var ign = new L.TileLayer.WMTS( url ,
        {
         layer: layerIGNScanStd,
         style: "normal",
         tilematrixSet: "PM",
         format: "image/jpeg",
         attribution: "<a href='https://github.com/mylen/leaflet.TileLayer.WMTS'>GitHub</a>&copy; <a href='http://www.ign.fr'>IGN</a>"
     }
     );
        //var map = L.map('map').setView([48.505, 3.09], 13);

        L.control.scale({'position':'bottomleft','metric':true,'imperial':false}).addTo(map);

        map.addLayer(ign);

        var baseLayers = {"Carte IGN" : ign};

        L.control.layers(baseLayers, {}).addTo(map);    
        console.log("ok")
    </script>
    <?php
    if( array_key_exists('batiment', $_GET) ){
        # Our new data
        echo "
        <script type=\"text/javascript\">
        var name_batiment = "."\"".$_GET["batiment"]."\"".";
        </script>
        ";
    }
    else{
        echo "
        <script type=\"text/javascript\">
        var name_batiment = \"0\";
        </script>
        ";
    }
    ?>

</body>
</html>


