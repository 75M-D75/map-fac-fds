<?php
	// Connexion, sélection de la base de données
	$dbconn = pg_connect("host=localhost port=5432 dbname=bdd user=postgres password=MDFXE755") or die('Connexion impossible : ' . pg_last_error());

	$indice_quartier = 0;
	$t = array();
	if( array_key_exists('coordX_1', $_POST) ){
		// Exécution de la requête SQL
		$coordX_1 = $_POST['coordX_1'];
		$coordY_1 = $_POST['coordY_1'];
		$coordX_2 = $_POST['coordX_2'];
		$coordY_2 = $_POST['coordY_2'];
		$query = 'SELECT ST_Distance(ST_Transform(\'SRID=4326;POINT('.$coordX_1.' '.$coordY_1.')\'::geometry, 3857),
 							ST_Transform(\'SRID=4326;POINT('.$coordX_2.' '.$coordY_2.')\'::geometry, 3857)
 							) * cosd(42.3521);';
		
		/*SELECT ST_Distance(ST_Transform('SRID=4326;POINT(12.5 14.6)'::geometry, 3857),
 							ST_Transform('SRID=4326;POINT(43.2 44.5)'::geometry, 3857)
 							) * cosd(42.3521);';*/

		$result = pg_query($query) or die('Échec de la requête : ' . pg_last_error());
		
		while($ligne = pg_fetch_array($result, null, PGSQL_ASSOC)){
		    foreach ($ligne as $col_value) {
		        $tab_quartier[$indice_quartier] = $col_value;
		        $indice_quartier = $indice_quartier + 1;
		    }
		}

	    $t['distance'] = $tab_quartier;
		
		// Libère le résultat
		pg_free_result($result);
	}
	echo json_encode($t);

	// Ferme la connexion
	pg_close($dbconn);
?>