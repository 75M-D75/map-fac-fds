<?php
	// Connexion, sélection de la base de données
	$dbconn = pg_connect("host=localhost port=5432 dbname=bdd user=postgres password=MDFXE755") or die('Connexion impossible : ' . pg_last_error());

	$tab_quartier = array();
	$indice_quartier = 0;
	$t = array();
	if( array_key_exists('nom_quartier', $_POST) ){
		$nom_quartier = $_POST['nom_quartier'];
		
		//Suppression de la vue si déja existante
		$query = 'DROP view bat_select;';
		pg_query($query) or die('Échec de la requête : ' . pg_last_error());

		//Creation de la vue des batiment se trouvant de le quartier (nom_quartier)
		$query = 'create view bat_select as 
		select id, geom 
		from (select ST_ContainsProperly(q.geom, ST_Boundary(b.geom)) as contains, b.geom, b.id from batis_m b, quartier_m q 
			where nom_iris=\''.$nom_quartier.'\') as tab_contains where contains=\'t\';';
		
		pg_query($query) or die('Échec de la requête : ' . pg_last_error());

		//Selection de la distance minimum pour
		//loisir
		$query = 'select MIN(dist) as min FROM
			(SELECT DISTINCT ON (bati.id) bati.id as id_bat, l.id as id_loisir, ST_Distance(l.geom, bati.geom) as dist 
			FROM batis_m As bati, bat_select bs,loisir As l
			where bs.id = bati.id
			ORDER BY bati.id, l.id, ST_Distance(l.geom, bati.geom)) as mini_dist;';
		
		$result = pg_query($query) or die('Échec de la requête : ' . pg_last_error());
		$ligne = pg_fetch_array($result, null, PGSQL_ASSOC);
		foreach ($ligne as $col_value) {
	        $tab_quartier[$indice_quartier] = $col_value;
	        $indice_quartier = $indice_quartier + 1;
	    }

	    //Selection de la distance minimum pour
		//hopitaux
		$query = 'select MIN(dist) as min FROM
(SELECT DISTINCT ON (bati.id) bati.id as id_bat, l.id as id_loisir, ST_Distance(l.geom, bati.geom) as dist 
FROM batis_m As bati, bat_select bs, hopitaux As l
where bs.id = bati.id
ORDER BY bati.id, l.id, ST_Distance(l.geom, bati.geom)) as mini_dist;';
		
		$result = pg_query($query) or die('Échec de la requête : ' . pg_last_error());
		$ligne = pg_fetch_array($result, null, PGSQL_ASSOC);
		foreach ($ligne as $col_value) {
	        $tab_quartier[$indice_quartier] = $col_value;
	        $indice_quartier = $indice_quartier + 1;
	    }

	    //Selection de la distance minimum pour
	    //etablissement
	    $query = 'select MIN(dist) as min FROM
(SELECT DISTINCT ON (bati.id) bati.id as id_bat, l.id as id_loisir, ST_Distance(l.geom, bati.geom) as dist 
FROM batis_m As bati, bat_select bs, etablissement As l
where bs.id = bati.id
ORDER BY bati.id, l.id, ST_Distance(l.geom, bati.geom)) as mini_dist;';
		
		$result = pg_query($query) or die('Échec de la requête : ' . pg_last_error());
		$ligne = pg_fetch_array($result, null, PGSQL_ASSOC);
		foreach ($ligne as $col_value) {
	        $tab_quartier[$indice_quartier] = $col_value;
	        $indice_quartier = $indice_quartier + 1;
	    }

	    //Selection de la distance minimum pour
	    //commercial
	    $query = 'select MIN(dist) as min FROM
(SELECT DISTINCT ON (bati.id) bati.id as id_bat, l.id as id_loisir, ST_Distance(l.geom, bati.geom) as dist 
FROM batis_m As bati, bat_select bs, commercial As l
where bs.id = bati.id
ORDER BY bati.id, l.id, ST_Distance(l.geom, bati.geom)) as mini_dist;';
		
		$result = pg_query($query) or die('Échec de la requête : ' . pg_last_error());
		$ligne = pg_fetch_array($result, null, PGSQL_ASSOC);
		foreach ($ligne as $col_value) {
	        $tab_quartier[$indice_quartier] = $col_value;
	        $indice_quartier = $indice_quartier + 1;
	    }

	    //Selection de la distance minimum pour
	    //transport
	    $query = 'select MIN(dist) as min FROM
(SELECT DISTINCT ON (bati.id) bati.id as id_bat, l.id as id_loisir, ST_Distance(l.geom, bati.geom) as dist 
FROM batis_m As bati, bat_select bs, transport As l
where bs.id = bati.id
ORDER BY bati.id, l.id, ST_Distance(l.geom, bati.geom)) as mini_dist;';
		
		$result = pg_query($query) or die('Échec de la requête : ' . pg_last_error());
		$ligne = pg_fetch_array($result, null, PGSQL_ASSOC);
		foreach ($ligne as $col_value) {
	        $tab_quartier[$indice_quartier] = $col_value;
	        $indice_quartier = $indice_quartier + 1;
	    }


	    //Selection de la distance minimum pour
	    //arret_tram
	    $query = 'select MIN(dist) as min FROM
(SELECT DISTINCT ON (bati.id) bati.id as id_bat, l.id as id_loisir, ST_Distance(l.geom, bati.geom) as dist 
FROM batis_m As bati, bat_select bs, arret_tram As l
where bs.id = bati.id
ORDER BY bati.id, l.id, ST_Distance(l.geom, bati.geom)) as mini_dist;';
		
		$result = pg_query($query) or die('Échec de la requête : ' . pg_last_error());
		$ligne = pg_fetch_array($result, null, PGSQL_ASSOC);
		foreach ($ligne as $col_value) {
	        $tab_quartier[$indice_quartier] = $col_value;
	        $indice_quartier = $indice_quartier + 1;
	    }
	    

	    /*select ST_AsGeoJSON(geom) from (select ST_ContainsProperly(q.geom, ST_Boundary(b.geom)) as contains, b.geom from batis_m b, quartier_m q where nom_iris='Bagatelle') as tab_contains where contains='t';*/
	    
		$t['geom'] = $tab_quartier;
		
		// Libère le résultat
		pg_free_result($result);
	}
	echo json_encode($t);

	// Ferme la connexion
	pg_close($dbconn);
?>