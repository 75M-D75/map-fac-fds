-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost
-- Généré le :  Dim 01 sep. 2019 à 15:17
-- Version du serveur :  5.7.23
-- Version de PHP :  7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `bdd`
--

-- --------------------------------------------------------

--
-- Structure de la table `commercial`
--

CREATE TABLE `commercial` (
  `gid` int(11) NOT NULL COMMENT 'TRIAL',
  `id` varchar(24) DEFAULT NULL COMMENT 'TRIAL',
  `origine` varchar(17) DEFAULT NULL COMMENT 'TRIAL',
  `nature` varchar(19) DEFAULT NULL COMMENT 'TRIAL',
  `toponyme` varchar(70) DEFAULT NULL COMMENT 'TRIAL',
  `importance` varchar(2) DEFAULT NULL COMMENT 'TRIAL',
  `geom` text COMMENT 'TRIAL',
  `trial572` char(1) DEFAULT NULL COMMENT 'TRIAL'
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='TRIAL';

--
-- Déchargement des données de la table `commercial`
--

INSERT INTO `commercial` (`gid`, `id`, `origine`, `nature`, `toponyme`, `importance`, `geom`, `trial572`) VALUES
(1, 'PAIINDUS0000000044348236', 'BDTopo', 'Divers industriel', 'NR', 'NR', 'POINT(3.90733545582561 43.617443626407)', 'T'),
(2, 'PAIINDUS0000000044348239', 'G?oroute', 'Zone industrielle', 'NR', 'NR', 'POINT(3.81940336515422 43.6202927363129)', 'T'),
(3, 'PAIINDUS0000000044348265', 'G?oroute', 'Zone industrielle', 'NR', 'NR', 'POINT(3.92215198746344 43.5963897866247)', 'T'),
(4, 'PAIINDUS0000000044348229', 'G?oroute', 'Zone industrielle', 'NR', 'NR', 'POINT(3.8263810714065 43.6368462818231)', 'T'),
(5, 'PAIINDUS0000000044348228', 'BDTopo', 'Divers commercial', 'NR', 'NR', 'POINT(3.81801138264148 43.6421952182226)', 'T'),
(6, 'PAIINDUS0000000044348240', 'BDTopo', 'March?', 'NR', 'NR', 'POINT(3.81716491914792 43.6275756902252)', 'T'),
(7, 'PAIINDUS0000000044348231', 'BDTopo', 'Divers commercial', 'NR', 'NR', 'POINT(3.82079385464924 43.6316002862295)', 'T'),
(8, 'PAIINDUS0000000044348251', 'BDTopo', 'Divers commercial', 'NR', 'NR', 'POINT(3.83988578392468 43.6118930939307)', 'T'),
(9, 'PAIINDUS0000000044348267', 'BDTopo', 'Divers commercial', 'NR', 'NR', 'POINT(3.85534033525633 43.5884852963067)', 'T'),
(10, 'PAIINDUS0000000044348238', 'BDTopo', 'Divers commercial', 'NR', 'NR', 'POINT(3.87055433079299 43.6265943213304)', 'T'),
(11, 'PAIINDUS0000000044348249', 'BDTopo', 'March?', 'NR', 'NR', 'POINT(3.87663785052234 43.6060914078832)', 'T'),
(12, 'PAIINDUS0000000044348246', 'BDTopo', 'March?', 'NR', 'NR', 'POINT(3.87681027482869 43.6101626215607)', 'T'),
(13, 'PAIINDUS0000000044348248', 'BDTopo', 'Divers commercial', 'NR', 'NR', 'POINT(3.88502838749916 43.6084052282173)', 'T'),
(14, 'PAIINDUS0000000211169903', 'Orthophotographie', 'Divers industriel', 'NR', 'NR', 'POINT(3.87342107391965 43.6384896900717)', 'T'),
(15, 'PAIINDUS0000000044348247', 'BDNyme', 'Divers commercial', 'le polygone', '8', 'POINT(3.88451927157227 43.6085560451078)', 'T'),
(16, 'PAIINDUS0000000044348230', 'G?oroute', 'Zone industrielle', 'parc eurom?decine', '6', 'POINT(3.83752205213897 43.6401714125169)', 'T'),
(17, 'PAIINDUS0000000044348245', 'G?oroute', 'Zone industrielle', 'le mill?naire', '7', 'POINT(3.913455133378 43.6124639766673)', 'T'),
(18, 'PAIINDUS0000000223410924', 'Fichier', 'Divers industriel', 'NR', 'NR', 'POINT(3.81670420455912 43.6200143103096)', 'T'),
(19, 'PAIINDUS0000000224271054', 'Orthophotographie', 'Divers industriel', 'NR', 'NR', 'POINT(3.84077294730271 43.6001777946578)', 'T'),
(20, 'PAIINDUS0000000215173526', 'Orthophotographie', 'Divers industriel', 'NR', 'NR', 'POINT(3.91466756363519 43.6178230854112)', 'T'),
(21, 'PAIINDUS0000000215173801', 'Orthophotographie', 'Divers industriel', 'NR', 'NR', 'POINT(3.91437709015871 43.6016978732837)', 'T');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `commercial`
--
ALTER TABLE `commercial`
  ADD PRIMARY KEY (`gid`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `commercial`
--
ALTER TABLE `commercial`
  MODIFY `gid` int(11) NOT NULL AUTO_INCREMENT COMMENT 'TRIAL', AUTO_INCREMENT=22;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
