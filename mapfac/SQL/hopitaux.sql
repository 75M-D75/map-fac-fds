-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost
-- Généré le :  jeu. 22 août 2019 à 00:57
-- Version du serveur :  5.7.23
-- Version de PHP :  7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `bdd`
--

-- --------------------------------------------------------

--
-- Structure de la table `hopitaux`
--

CREATE TABLE `hopitaux` (
  `gid` int(11) NOT NULL COMMENT 'TRIAL',
  `id` varchar(24) DEFAULT NULL COMMENT 'TRIAL',
  `origine` varchar(17) DEFAULT NULL COMMENT 'TRIAL',
  `nature` varchar(25) DEFAULT NULL COMMENT 'TRIAL',
  `toponyme` varchar(70) DEFAULT NULL COMMENT 'TRIAL',
  `importance` varchar(2) DEFAULT NULL COMMENT 'TRIAL',
  `geom` text COMMENT 'TRIAL',
  `trial572` char(1) DEFAULT NULL COMMENT 'TRIAL'
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='TRIAL';

--
-- Déchargement des données de la table `hopitaux`
--

INSERT INTO `hopitaux` (`gid`, `id`, `origine`, `nature`, `toponyme`, `importance`, `geom`, `trial572`) VALUES
(1, 'PAISANTE0000000044347691', 'G?oroute', 'H?pital', 'NR', 'NR', 'POINT(3.86201668975159 43.629877473071)', 'T'),
(2, 'PAISANTE0000000044347701', 'BDTopo', 'Etablissement hospitalier', 'bionne', '8', 'POINT(3.82082862764008 43.6023494804166)', 'T'),
(3, 'PAISANTE0000000044347685', 'BDTopo', 'Etablissement hospitalier', 'NR', 'NR', 'POINT(3.83199806659348 43.6429637337004)', 'T'),
(4, 'PAISANTE0000000044347678', 'BDTopo', 'Etablissement hospitalier', 'NR', 'NR', 'POINT(3.86365806074092 43.6418845034964)', 'T'),
(5, 'PAISANTE0000000044347696', 'BDTopo', 'Etablissement hospitalier', 'NR', 'NR', 'POINT(3.81709761564319 43.6295468229601)', 'T'),
(6, 'PAISANTE0000000044347703', 'BDTopo', 'Etablissement hospitalier', 'NR', 'NR', 'POINT(3.82856690554815 43.6015385012335)', 'T'),
(7, 'PAISANTE0000000044347679', 'BDTopo', 'Etablissement hospitalier', 'NR', 'NR', 'POINT(3.85890324254085 43.640611098657)', 'T'),
(8, 'PAISANTE0000000044347706', 'BDTopo', 'Etablissement hospitalier', 'NR', 'NR', 'POINT(3.83367470082566 43.6002756823722)', 'T'),
(9, 'PAISANTE0000000044347702', 'BDTopo', 'Etablissement hospitalier', 'NR', 'NR', 'POINT(3.83778603834595 43.6104938827818)', 'T'),
(10, 'PAISANTE0000000044347680', 'BDTopo', 'Etablissement hospitalier', 'NR', 'NR', 'POINT(3.85146308140045 43.6397976744464)', 'T'),
(11, 'PAISANTE0000000044347690', 'BDTopo', 'Etablissement hospitalier', 'NR', 'NR', 'POINT(3.85719181950961 43.6226044835119)', 'T'),
(12, 'PAISANTE0000000044347705', 'BDTopo', 'Etablissement hospitalier', 'NR', 'NR', 'POINT(3.86209994067185 43.5923955307724)', 'T'),
(13, 'PAISANTE0000000044347700', 'BDTopo', 'Etablissement hospitalier', 'NR', 'NR', 'POINT(3.87021486103266 43.6102615287555)', 'T'),
(14, 'PAISANTE0000000044347676', 'BDTopo', 'Etablissement hospitalier', 'NR', 'NR', 'POINT(3.87488641362057 43.6310731211229)', 'T'),
(15, 'PAISANTE0000000044347699', 'BDTopo', 'H?pital', 'NR', 'NR', 'POINT(3.84990087948543 43.6095494760738)', 'T'),
(16, 'PAISANTE0000000044347698', 'BDTopo', 'H?pital', 'NR', 'NR', 'POINT(3.85219676243792 43.6129634210374)', 'T'),
(17, 'PAISANTE0000000044347695', 'BDTopo', 'H?pital', 'NR', 'NR', 'POINT(3.86415622073107 43.6199353271514)', 'T'),
(18, 'PAISANTE0000000044347694', 'BDTopo', 'H?pital', 'NR', 'NR', 'POINT(3.8686473055882 43.6155128587192)', 'T'),
(19, 'PAISANTE0000000044347693', 'BDTopo', 'H?pital', 'NR', 'NR', 'POINT(3.87185013227516 43.6192646956208)', 'T'),
(20, 'PAISANTE0000000044347684', 'G?oroute', 'H?pital', 'NR', 'NR', 'POINT(3.83633244930319 43.6439005088261)', 'T'),
(21, 'PAISANTE0000000044347683', 'G?oroute', 'H?pital', 'NR', 'NR', 'POINT(3.83914206232327 43.6438527660811)', 'T'),
(22, 'PAISANTE0000000044347682', 'BDTopo', 'H?pital', 'NR', 'NR', 'POINT(3.8345959948497 43.6376545704143)', 'T'),
(23, 'PAISANTE0000000044347692', 'BDTopo', 'H?pital', 'saint-?loi', '8', 'POINT(3.86480759393464 43.6291282381439)', 'T'),
(24, 'PAISANTE0000000044347681', 'BDTopo', 'Etablissement hospitalier', 'clinique du val d\'aurelle', '8', 'POINT(3.83724698778577 43.643820583368)', 'T'),
(25, 'PAISANTE0000000220102844', 'Orthophotographie', 'Etablissement hospitalier', 'NR', 'NR', 'POINT(3.83811250375047 43.6393696052187)', 'T'),
(26, 'PAISANTE0000000220102853', 'Orthophotographie', 'H?pital', 'NR', '7', 'POINT(3.84983061361561 43.6318936723436)', 'T'),
(27, 'PAISANTE0000000220102866', 'Orthophotographie', 'H?pital', 'NR', '7', 'POINT(3.85143945748279 43.6298618531827)', 'T'),
(28, 'PAISANTE0000000223409032', 'Fichier', 'Etablissement hospitalier', 'NR', 'NR', 'POINT(3.85529064183602 43.6287626165568)', 'T'),
(29, 'PAISANTE0000000223410973', 'Fichier', 'Etablissement hospitalier', 'NR', 'NR', 'POINT(3.85420286081958 43.6285048190818)', 'T'),
(30, 'PAISANTE0000000044347688', 'BDTopo', 'Etablissement hospitalier', 'la colombi?re', '8', 'POINT(3.85601253045086 43.626665720534)', 'T'),
(31, 'PAISANTE0000000044347677', 'BDTopo', 'H?pital', 'h?pital lapeyronie', '7', 'POINT(3.85069001264899 43.6313677454687)', 'T'),
(32, 'PAISANTE0000000220116650', 'Orthophotographie', 'Etablissement hospitalier', 'NR', '8', 'POINT(3.91372590765926 43.6014720963928)', 'T');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `hopitaux`
--
ALTER TABLE `hopitaux`
  ADD PRIMARY KEY (`gid`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `hopitaux`
--
ALTER TABLE `hopitaux`
  MODIFY `gid` int(11) NOT NULL AUTO_INCREMENT COMMENT 'TRIAL', AUTO_INCREMENT=33;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
